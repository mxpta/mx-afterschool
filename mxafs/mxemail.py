import os, sys, string, socket

import logging

from flask import Flask, render_template

import smtplib

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

import model

# Configuration
class EmailConfig:
    pass

__cfg = EmailConfig()
__cfg.server = 'localhost'
__cfg.port = 5757
__cfg.mailuser = None
__cfg.mailpasswd = None

def setServer( smtpServer, port, mailuser = None, mailpasswd = None ):
    __cfg.server = smtpServer
    __cfg.port = int(port)
    __cfg.mailuser = mailuser
    __cfg.mailpasswd = mailpasswd

def doSendEmail(toAddr, subject, htmlBody, textBody, emailtype ):
    fromAddr = 'afterschool@mxpta.org'

    # Create message container - the correct MIME type is multipart/alternative.
    msg = MIMEMultipart('alternative')
    msg['Subject'] = unicode( subject, 'utf-8')
    msg['To'] = toAddr
    msg['From'] = 'MX PTA Afterschool <afterschool@ase.mxpta.org>'

    # Create the body of the message (a plain-text and an HTML version).
    text = textBody
    html = htmlBody

    # Record the MIME types of both parts - text/plain and text/html.
    part1 = MIMEText( text.encode('utf-8'), 'plain')
    part2 = MIMEText( html.encode('utf-8'), 'html')

    # Attach parts into message container.
    # According to RFC 2046, the last part of a multipart message, in this case
    # the HTML message, is best and preferred.
    msg.attach(part1)
    msg.attach(part2)

    # Send the message via local SMTP server, port 587
    print "SMTP config: ", __cfg.server, __cfg.port
    try:
        s = smtplib.SMTP( __cfg.server, __cfg.port )

        if __cfg.mailuser:
            s.login( __cfg.mailuser, __cfg.mailpasswd )

        # sendmail function takes 3 arguments: sender's address, recipient's address
        # and message to send - here it is sent as one string.
        s.sendmail(fromAddr, toAddr, msg.as_string())
        s.quit()

        #logging.info( "Success sending email to %s -- %s", toAddr, emailtype )

        return True

    except (smtplib.SMTPAuthenticationError, socket.error) as e:
        logging.error( "Error sending email %s '%s' -- %s, %s" , toAddr, emailtype, type(e), str(e) )
        return False



def sendWelcomeEmail(user, calendar):
    return doSendEmail(user.fancyEmail(), "MX PTA Afterschool Welcomes You!",
                render_template('email/welcome.html', user=user, calendar=calendar ),
                render_template('email/welcome.txt', user=user, calendar=calendar ),
                "Welcome")


def sendRemindEmail(user):
    return doSendEmail(user.fancyEmail(), "MX PTA Afterschool Login Link!",
                render_template('email/remind.html', user=user),
                render_template('email/remind.txt', user=user),
                "Remind")


def sendInvoiceEmail(user, reqs, totalDue, calendar ):
    htmlInvoice = render_template('email/invoice.html', user=user, reqs=reqs, total=totalDue, calendar=calendar )

    textInvoice = render_template('email/invoice.txt', user=user, reqs=reqs, total=totalDue, calendar=calendar )

    return doSendEmail(user.fancyEmail(), "Invoice -- Afterschool Classes",
                htmlInvoice, textInvoice, "Invoice" )


def sendReceiptEmail(user, reqs, calendar ):
    htmlInvoice = render_template('email/receipt.html', user=user, reqs=reqs, calendar=calendar )
    textInvoice = render_template('email/receipt.txt', user=user, reqs=reqs, calendar=calendar )

    return doSendEmail(user.fancyEmail(), "Payment Received -- Afterschool Classes",
                htmlInvoice, textInvoice, "Receipt" )

