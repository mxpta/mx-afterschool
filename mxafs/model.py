# -*- coding: utf-8 -*-

import os, sys
import string, random
import datetime
import json
import requests
import random
#import faker
import unicodedata, codecs

from pprint import pprint

from flask.ext.sqlalchemy import SQLAlchemy
#from flask.ext.sqlalchemy.orm import backref, relationship

import mxinvoice

db = SQLAlchemy()

WEEKDAYS = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"]

DAY_ABBR = {"mon": "Monday",
            "tue": "Tuesday",
            "wed": "Wednesday",
            "thu": "Thursday",
            "fri": "Friday",
            "all": "All"}

# -----------------------------------------------------------
#  Utilities
# -----------------------------------------------------------
def genPasswd(length=8, chars=string.letters + string.digits):
    return ''.join([random.choice(chars) for i in range(length)])

# -----------------------------------------------------------
#  Config
# -----------------------------------------------------------
class Settings(db.Model):
    id = db.Column( db.Integer, primary_key=True )

    semester = db.Column( db.String(100) )
    reg_enabled = db.Column( db.Boolean() )

def getSettings():
    return Settings.query.first()

# -----------------------------------------------------------
#  CalendarEvent -- events for the FAQ calendar
# -----------------------------------------------------------
class CalendarEvent(db.Model):
    id = db.Column( db.Integer, primary_key=True )

    title = db.Column( db.String(100) )
    desc = db.Column( db.String(140) )
    date = db.Column( db.DateTime() )

    minical = db.Column( db.Boolean() )

    # FirstDay - First day of classes*
    # LastDay - Last day of classes*
    # Deadline - A deadline for something*
    # Holiday - No classes on this day
    # General - General FAQ entry*
    # Dates marked with * are shown on the "Important Dates" on the FAQ
    role = db.Column(db.Enum("FirstDay", "LastDay", "Deadline", "Holiday", "General",
                                name='calendar_role') )

    def fmtDateShort(self):
        "e.g. Feb. 3"
        return self.date.strftime( "%b. %-d")

    def fmtDateHoliday(self):
        "e.g. Mon, Feb. 3"
        return self.date.strftime( "%a, %b. %-d")

    def fmtDate(self):
        "e.g. Monday, February 3"
        return self.date.strftime("%A, %B %-d")

def calendarDates():
    """Helper that gets all calendar dates indexed by name"""

    # stuff calendar dates into an array in case the page wants them
    calendar = {}
    for calEvent in CalendarEvent.query.all():
        calendar[calEvent.title] = calEvent

    return calendar

# -----------------------------------------------------------
#  CourseInfo -- common info for a course that may have
#   multiple sessions
# -----------------------------------------------------------
class CourseInfo(db.Model):
    id = db.Column( db.Integer, primary_key=True )
    title = db.Column(db.String(100) )
    picurl = db.Column(db.String(120))
    slug = db.Column(db.String(30))

    summary = db.Column(db.String(500))
    desc = db.Column(db.Text())

    courses = db.relationship('Course', backref='course_info',
                            lazy='dynamic')

    def __repr__(self):
        return '<CourseInfo %r>' % self.title

    def __unicode__(self):
        return self.title


# -----------------------------------------------------------
#  Course
# -----------------------------------------------------------
course_students = db.Table('course_students',
                           db.Column('student_id', db.Integer, db.ForeignKey('student.id')),
                           db.Column('course_id', db.Integer, db.ForeignKey('course.id'))
)

class Course(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    classgroup = db.Column(db.String(15))
    weekday = db.Column(db.Enum("Monday", "Tuesday", "Wednesday",
                                "Thursday", "Friday", name='weekday_enum'), nullable=False)
    starttime = db.Column(db.Time(timezone=True))
    duration = db.Column(db.Integer())

    instructor_id = db.Column(db.Integer, db.ForeignKey('instructor.id'))
    courseinfo_id = db.Column(db.Integer, db.ForeignKey('course_info.id'))

    minimum = db.Column(db.Integer())
    capacity = db.Column(db.Integer())
    fee = db.Column(db.Integer())

    waitlist = db.Column(db.Boolean())

    mingrade = db.Column(db.Integer())
    maxgrade = db.Column(db.Integer())

    room = db.Column(db.String(15))

    def dayAbbr(self):
        return self.weekday[:3].upper()

    def abbr(self):
        return ''.join(map(lambda x: x[0], self.title.split()))

    def _gradeAbbr(self, grade):
        gradeAbbr = { 0:'TK/K', 1:'1st', 2:'2nd', 3:'3rd', 4:'4th', 5:'5th'}
        return gradeAbbr.get( grade, str(grade))

    def _gradeAbbr2(self, grade):
        """Like gradeAbbr, but shortens TK for mingrades"""
        gradeAbbr = { 0:'TK', 1:'1st', 2:'2nd', 3:'3rd', 4:'4th', 5:'5th'}
        return gradeAbbr.get( grade, str(grade))

    def titleGrade(self):

        if self.mingrade == self.maxgrade:
            return   "%s (%s)" % (self.course_info.title, self._gradeAbbr(self.mingrade))
        else:
            return "%s (%s-%s)" % (self.course_info.title, self._gradeAbbr2(self.mingrade), self._gradeAbbr(self.maxgrade))

    def _fmtTime(self, timeval ):

            timestr = timeval.strftime('%I:%M')

            # remove annoying zero-padding
            if (timestr.startswith('0')):
                timestr = timestr[1:]

            return timestr

    def _fmtTimeAMPM(self, timeval ):

            timestr = timeval.strftime('%I:%M %p')

            # remove annoying zero-padding
            if (timestr.startswith('0')):
                timestr = timestr[1:]

            return timestr


    def displayTime(self):
            if self.starttime:
                timestr = self._fmtTime( self.starttime )

                if self.duration:
                    tm = self.starttime
                    fulldate = datetime.datetime(2000, 1, 1, tm.hour, tm.minute, tm.second)
                    endtime = fulldate + datetime.timedelta( minutes=self.duration )

                    timestr += " - " + self._fmtTime( endtime )
            else:
                timestr = ""
            return timestr

    def displayTime2(self):
            if self.starttime:
                timestr = self._fmtTimeAMPM( self.starttime )

            else:
                timestr = ""
            return timestr


    def countReq(self):
        return len( self.requests)

    def countAlt(self):
        return len( self.altrequests )

    def countRegistered(self):
        return self._countStatus( 'registered')

    def countEnrolled(self):
        return self._countStatus( 'enrolled')

    def _countStatus(self, status):
        count = 0
        for cr in self.requests:
            if cr.status==status:
                count += 1
        return count

    def __repr__(self):
        return '<Course %d>' % self.id

    def __unicode__(self):
        cinfo = self.course_info
        if (cinfo):
            return "%s-%d" % (cinfo.slug, self.id)
        else:
            return "course-%d" % (self.id)

# -----------------------------------------------------------
#  Classroom
# -----------------------------------------------------------
class Classroom( db.Model ):
    """This represents a student's daytime classroom and teacher. It is
    used for reports for planning how to get students to their afterschool
    classes, and to distribute afterschool materials to teachers and parent
    thru their classroom mailbox.
    """
    id = db.Column(db.Integer, primary_key=True)
    instname = db.Column(db.String(80))
    room = db.Column(db.String(40))
    grade = db.Column(db.Integer())

    students = db.relationship('Student', backref='classroom',
                                lazy='dynamic')

    def gradeAbbr(self):
        gradeAbbr = { 0:'TK/K', 1:'1st', 2:'2nd', 3:'3rd', 4:'4th', 5:'5th'}
        return gradeAbbr.get( self.grade, str(self.grade))

    def __repr__(self):
        return '<Classroom %s>' % self.instname

# -----------------------------------------------------------
#  Instructor
# -----------------------------------------------------------
class Instructor (db.Model):

    id = db.Column(db.Integer, primary_key=True)
    instname = db.Column(db.String(80))
    email = db.Column(db.String(120))
    phone = db.Column(db.String(80))
    bio = db.Column(db.Text())
    rate = db.Column( db.Float() )

    courses = db.relationship('Course', backref='instructor',
                                lazy='dynamic')

    def shortName(self):
        shortname =  string.split( self.instname )[-1]
        shortname =  string.split( shortname, '-' )[-1]
        return shortname

    def __repr__(self):
        return '<Instructor %r>' % self.instname

    def __unicode__(self):
        return self.instname


# -----------------------------------------------------------
#  Student
# -----------------------------------------------------------
class Student(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    firstname = db.Column(db.String(80))
    lastname = db.Column(db.String(80))
    grade = db.Column(db.Integer)
    enrollDays = db.Column(db.Integer)
    teacher = db.Column(db.String(80))

    # Aftercare?
    bears = db.Column(db.Boolean())
    learns = db.Column(db.Boolean())
    afcroom = db.Column(db.String(20)) # Aftercare (LEARNS/BEARS) Room #

    classroom_id = db.Column(db.Integer, db.ForeignKey('classroom.id'))

    courses = db.relationship('Course', secondary=course_students,
                              backref=db.backref('students', lazy='dynamic'))

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def displayname(self):
        return unicode(self.firstname) + " " + unicode(self.lastname)

    def reqsForDay(self, weekday ):
        reqs = []
        for cr in self.requests:
            if cr.course.weekday == weekday:
                reqs.append( cr )
        return reqs

    def numSlots(self):
        return len(self.requests)

    def gradetext(self):
        if self.grade == 0:
            return "Kindergarten/TK"
        else:
            return "Grade %d" % self.grade

    def gradeabbr(self):
        gradeAbbrs = ["K/TK", "1st", "2nd", "3rd", "4th", "5th"]
        return gradeAbbrs[self.grade]

    def aftercareRoom(self):


        # Do we have an aftercare room assigned?
        if self.afcroom:
            return self.afcroom
        elif self.bears:
            return 'BEARS'
        elif self.learns:
            return 'Learns'

        return None


    def __repr__(self):
        return '<Student %r %r>' % (self.firstname, self.lastname)

    def __unicode__(self):
        return unicode(self.firstname) + ' ' + unicode(self.lastname)


# -----------------------------------------------------------
#  User (parent or admin)
# -----------------------------------------------------------
class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    firstname = db.Column(db.String(80))
    lastname = db.Column(db.String(80))
    phone = db.Column(db.String(80))
    passwd = db.Column(db.String(80))  # hashed password
    email = db.Column(db.String(120), unique=True)

    # Scholarship info
    scholarship = db.Column(db.Boolean())
    scholrequest = db.Column(db.Integer())
    scholnote = db.Column(db.Text())
    scholgrant = db.Column(db.Integer())
    scholapply = db.Column(db.Integer())

    invoicesent = db.Column(db.DateTime(timezone=True))

    students = db.relationship("Student", backref="user")

    payments = db.relationship("Payment", backref="user")


    def displayname(self):
        return self.firstname + " " + self.lastname

    # Sort of hacky, just used to preview invoice in admin
    def invoiceReqsHtml(self):
        regReqsForStu, totalDue = mxinvoice.gatherInvoicedCoursesForUser(self)

        html = '<table class="table table-bordered">'
        html += "<tr><th>Class</th><th>Student</th><th>Fee</th>"
        for req in regReqsForStu:
            html += "<tr><td>" + req.course.titleGrade() +"</td>"
            html += "<td>" + req.student.displayname() + "</td>"
            if req.status=='enrolled':
                html += "<td>PAID</td>"
            else:
                html += "<td> $"+str(req.course.fee)+"</td><tr>\n"

        if (self.scholgrant):
            html += "<tr><td colspan='2'>Scholarship</td><td> $("+str(self.scholgrant)+")</td></tr>\n"

        html += "<tr><td colspan='2'>Total Due:</td><td><b>$"+str(totalDue)+"</b></td></tr>"
        html += "</table>"


        return html

    def fancyEmail(self):
        return "%s <%s>" % (self.displayname(), self.email)

    def totalReqs(self):

        totalReqs = 0
        for stu in self.students:
            totalReqs += stu.numSlots()

        return totalReqs

    def totalEnrollDays(self):
        totalDays = 0
        for stu in self.students:
            if stu.enrollDays:
                totalDays += stu.numSlots()
        return totalDays

    def invoiceSentDesc(self):

        if self.invoicesent:
            return self.invoicesent.strftime('%a %b %d, %I:%M %p')
        else:
            return "Never"

    def __repr__(self):
        return '<User %r>' % self.email

    def __unicode__(self):
        return unicode(self.email)


# -----------------------------------------------------------
#  Course Request
# -----------------------------------------------------------
class CourseRequest(db.Model):

    id = db.Column(db.Integer, primary_key=True)

    # Requested - User signed up for the course
    # Provisional - Temporarily enrolled for scheduling, admin only
    # Registered - Assigned the course but not yet paid
    # Enrolled - Registered and paid
    # Waitlist - On waiting list for course, (likely no second choice)
    # Dropped - User asked to be removed, or admin dropped request for some reason (e.g. non-payment)
    #
    # Note: Students who got their second choice will be marked with 'seconded' flag and
    # course and altcourse will be swapped
    status = db.Column(db.Enum("requested", "provisional",
                               "registered", "enrolled",
                               "waitlist", "dropped",
                               name='status_enum'), nullable=False)

    # Waitlist sort key, arbitrary
    waitlist = db.Column( db.Integer )

    # If second choice was granted, course and altcourse will be swapped
    # (e.g. altcourse will be their original first choice) and this flag
    # will be set to mark that.
    seconded = db.Column( db.Boolean )

    student_id = db.Column(db.Integer, db.ForeignKey('student.id'))
    student = db.relationship(Student, backref=db.backref("requests"))

    course_id = db.Column(db.Integer, db.ForeignKey('course.id'))
    course = db.relationship(Course, backref=db.backref("requests", order_by=status), foreign_keys=[course_id] )

    altcourse_id = db.Column(db.Integer, db.ForeignKey('course.id'))
    altcourse = db.relationship(Course, backref=db.backref("altrequests", order_by=status), foreign_keys=[altcourse_id] )

    def statusColor(self):
        # Maps statuses to bootstrap colors
        colorForStatus = {
            "requested" : "default",
            "provisional": "info",
            "registered" : "success",
            "enrolled" : "success",
            "waitlist" : "warning",
            "dropped" : "danger"
        }

        return colorForStatus[ self.status ]

    def __repr__(self):
        return '<CourseRequest %d>' % self.id

    def __unicode__(self):

        # TODO: make this more useful
        return "CR-%d" % self.id


# -----------------------------------------------------------
#  Payment
# -----------------------------------------------------------
class Payment(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    datetime = db.Column(db.DateTime(timezone=True))

    # info we get from stripe
    stripeTokenType = db.Column(db.String(20))
    stripeToken = db.Column(db.String(80))
    stripeEmail = db.Column(db.String(80))  # email they used to pay with

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    amount = db.Column(db.Integer)


# -----------------------------------------------------------
#  Wipe and Rebuild DB
# -----------------------------------------------------------
def getBool( s ):
    if s.lower() in ['true', 'yes', '1']:
        return True
    else:
        return False

def rebuild_course_data( classDataPath ):
    print("POPULATE: generating course data...")

    # Get course data from json file
    course_fp = open( os.path.join( classDataPath, "courses.json") )
    coursedata = json.load(course_fp)
    course_fp.close()

    for course in coursedata["courses"]:
        c = Course()
        c.title = course["title"]
        c.weekday = course.get("weekday", "Monday")
        c.capacity = course.get("minimum", 6)
        c.capacity = course.get("capacity", 12)
        c.instemail = course.get("instemail", "")
        c.fee = course.get("fee", 0)
        c.featured = getBool( course.get("featured", "True") )
        c.mingrade = course.get("mingrade", 0 )
        c.maxgrade = course.get("maxgrade", 0 )

        if course.has_key("desc") and course["desc"]:
            print "desc is ", course["desc"]
            # c.desc = course["desc"]
            desc = codecs.open( os.path.join( classDataPath, course["desc"]), encoding='utf-8').read()
            unicodedata.normalize( 'NFKD', desc ).encode('ascii', 'ignore')
            c.desc = desc
        else:
            # No description text, use placeholder text
            r = requests.get("http://loripsum.net/api/plaintext/headers")
            c.desc = r.text

        if course.has_key("summary"):
            c.summary = course["summary"]
        else:
            c.summary = "No information available."

        db.session.add(c)
    db.session.commit()

def rebuild_db2():
    db.drop_all()
    db.create_all()

def rebuild_db_OLD( classDataPath, populate):
    print "db is ", db

    db.drop_all()
    db.create_all()

    # Import the bare course data
    rebuild_course_data( classDataPath )

    # Populate with test data?
    if not populate:
        return

    fake = faker.Faker()

    # Populate with test users and students
    print("POPULATE: generating users and students...")
    for i in range(50):

        user = User()
        user.lastname = fake.last_name()
        user.firstname = fake.first_name()
        user.email = fake.email()
        user.passwd = genPasswd()

        if random.randint(0, 10) == 0:
            user.bears = True

        if random.randint(0, 10) == 0:
            user.scholarship = True

        db.session.add(user)

        # Make some students for this user
        numChilds = random.randint(0, 3)
        for c in range(numChilds):
            student = Student()
            student.lastname = user.lastname
            student.firstname = fake.first_name()
            student.grade = random.randint(0, 5)
            student.user = user
            student.enrollDays = random.randint(1, 3)
            db.session.add(student)

    db.session.commit()

    print("POPULATE: generating course requests...")
    courses = Course.query.all()

    days = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday']

    # courseMon = Course.query.filter( Course.weekday=='Monday')
    # courseWed = Course.query.filter( Course.weekday=='Wednesday')
    # courseFri = Course.query.filter( Course.weekday=='Friday')

    # Now request a lot of courses
    students = Student.query.all()
    for stu in students:
        if stu.enrollDays == 1:
            # first, second from all week
            creq = CourseRequest()
            creq.student = stu
            creq.course = random.choice(courses)
            creq.altcourse = random.choice(courses)
            creq.status = "requested"
            db.session.add(creq)
        else:
            if stu.enrollDays == 2:
                skipDay = random.choice(days)
            else:
                skipDay = 'Nope'

            for day in days:
                if day == skipDay:
                    continue

                courseDay = Course.query.filter(Course.weekday == day)

                # first, second from this day
                creq = CourseRequest()
                creq.student = stu
                creq.course = random.choice(list(courseDay))
                creq.altcourse = random.choice(list(courseDay))
                creq.status = "requested"
                db.session.add(creq)

        db.session.commit()

    print("POPULATE: done...")






