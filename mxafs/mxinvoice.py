# Helpers for building invoices
import model
import datetime
import logging

from sqlalchemy import or_, and_

def compareReq(a, b):

    wdA = model.WEEKDAYS.index(a.course.weekday)
    wdB = model.WEEKDAYS.index(b.course.weekday)
    if wdA < wdB:
        return -1
    elif wdA > wdB:
        return 1
    else:
        # weekdays equal, use student
        return a.student_id - b.student_id


def gatherInvoicedCoursesForUser(user):
    # Get a list of registered courses
    regReqsForStu = model.CourseRequest.query.join(model.Student). \
        filter(and_(model.Student.user == user,
                    or_(model.CourseRequest.status == 'registered',
                        model.CourseRequest.status == 'enrolled')))


    # Sort by days, students
    regReqsForStu = list(regReqsForStu)
    regReqsForStu.sort(cmp=compareReq)

    # Apply scholarship grant, if availabe
    if user.scholgrant:
        totalDue = -user.scholgrant
    else:
        totalDue = 0

    for r in regReqsForStu:
        if r.status == 'registered':
            totalDue += r.course.fee

        #print r.student.displayname(), r.course.title, r.course.weekday, r.status


    return (regReqsForStu, totalDue )

def processPayment( user, amount, stripeInfo, reqs ):

    # unpack stripeinfo
    stripeTokenType, stripeToken, stripeEmail = stripeInfo

    payment = model.Payment()
    payment.user = user
    payment.amount = amount
    payment.datetime = datetime.datetime.now()
    payment.stripeTokenType = stripeTokenType
    payment.stripeToken = stripeToken
    payment.stripeEmail = stripeEmail

    model.db.session.add(payment)
    model.db.session.commit()

    # Apply the scholarship, if any
    if user.scholgrant:
        #logging.info("Applying scholarship %d for user %s" %(user.scholgrant, user.displayname()) )
        user.scholapply = user.scholgrant
        user.scholgrant = None
        model.db.session.commit()


    # Mark all the reqs as enrolled
    for req in reqs:
        if (req.status == 'registered'):
            #logging.info("Payment: Registered student %s into course %s", req.student.displayname(), req.course.titleGrade() )
            req.status = 'enrolled'
            req.course.students.append(req.student)

    model.db.session.commit()

def enrollWithoutPayment( reqs ):
    # Adds student to classes without marking them as paid. This is
    # for "IOU" situations where we have made arragements for people to
    # pay later, and we want to enroll (so classes/rosters can be generated)
    # but still track them as unpaid since they will pay past the deadline.
    for req in reqs:
        if (req.status=='registered'):
            # Don't change req status to enrolled
            req.course.students.append( req.student)

    model.db.session.commit()