import os, sys
import string, random
import json
import requests
import random
#import faker
import unicodedata, codecs

from pprint import pprint

from flask.ext.sqlalchemy import SQLAlchemy
# from flask.ext.sqlalchemy.orm import backref

import mxinvoice

db = SQLAlchemy()

WEEKDAYS = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday"]

DAY_ABBR = {"mon": "Monday",
            "tue": "Tuesday",
            "wed": "Wednesday",
            "thu": "Thursday",
            "fri": "Friday",
            "all": "All"}

# -----------------------------------------------------------
#  Utilities
# -----------------------------------------------------------
def genPasswd(length=8, chars=string.letters + string.digits):
    return ''.join([random.choice(chars) for i in range(length)])

# -----------------------------------------------------------
#  Course
# -----------------------------------------------------------
course_students = db.Table('course_students',
                           db.Column('student_id', db.Integer, db.ForeignKey('student.id')),
                           db.Column('course_id', db.Integer, db.ForeignKey('course.id'))
)


class Course(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100) )
    slug = db.Column(db.String(30))
    classgroup = db.Column(db.String(15))
    weekday = db.Column(db.Enum("Monday", "Tuesday", "Wednesday",
                                "Thursday", "Friday", name='weekday_enum'), nullable=False)
    starttime = db.Column(db.Time(timezone=True))
    duration = db.Column(db.Integer())
    instructor = db.Column(db.String(80))
    instemail = db.Column(db.String(120))
    picurl = db.Column(db.String(120))
    summary = db.Column(db.String(500))
    desc = db.Column(db.Text())
    instbio = db.Column(db.Text())
    minimum = db.Column(db.Integer())
    capacity = db.Column(db.Integer())
    fee = db.Column(db.Integer())
    featured = db.Column( db.Boolean(), default=True )

    waitlist = db.Column(db.Boolean())

    mingrade = db.Column(db.Integer())
    maxgrade = db.Column(db.Integer())

    def abbr(self):
        return ''.join(map(lambda x: x[0], self.title.split()))

    def _gradeAbbr(self, grade):
        gradeAbbr = { 0:'TK/K', 1:'1st', 2:'2nd', 3:'3rd', 4:'4th', 5:'5th'}
        return gradeAbbr.get( grade, str(grade))

    def _gradeAbbr2(self, grade):
        """Like gradeAbbr, but shortens TK for mingrades"""
        gradeAbbr = { 0:'TK', 1:'1st', 2:'2nd', 3:'3rd', 4:'4th', 5:'5th'}
        return gradeAbbr.get( grade, str(grade))

    def countReq(self, priority):
        count = 0
        for cr in self.requests:
            if cr.priority == priority:
                count += 1
        return count

    def titleGrade(self):

        if self.mingrade == self.maxgrade:
            return   "%s (%s)" % (self.title, self._gradeAbbr(self.mingrade))
        else:
            return "%s (%s-%s)" % (self.title, self._gradeAbbr2(self.mingrade), self._gradeAbbr(self.maxgrade))

    def __repr__(self):
        return '<Course %r>' % self.title

    def __unicode__(self):
        return self.title

# -----------------------------------------------------------
#  Instructor
# -----------------------------------------------------------
class Instructor (db.Model):

    id = db.Column(db.Integer, primary_key=True)
    instname = db.Column(db.String(80))
    email = db.Column(db.String(120))
    bio = db.Column(db.Text())

    def __repr__(self):
        return '<Instructor %r>' % self.instname

    def __unicode__(self):
        return self.title


# -----------------------------------------------------------
#  Student
# -----------------------------------------------------------
class Student(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    firstname = db.Column(db.String(80))
    lastname = db.Column(db.String(80))
    grade = db.Column(db.Integer)
    enrollDays = db.Column(db.Integer)
    teacher = db.Column(db.String(80))
    afcroom = db.Column(db.String(20)) # Aftercare (LEARNS/BEARS) Room #

    courses = db.relationship('Course', secondary=course_students,
                              backref=db.backref('students', lazy='dynamic'))

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def displayname(self):
        return self.firstname + " " + self.lastname

    def reqsForDay(self, weekday ):
        reqs = []
        for cr in self.requests:
            if cr.course.weekday == weekday:
                reqs.append( cr )
        return reqs

    def gradetext(self):
        if self.grade == 0:
            return "Kindergarten/TK"
        else:
            return "Grade %d" % self.grade

    def gradeabbr(self):
        gradeAbbrs = ["K/TK", "1st", "2nd", "3rd", "4th", "5th"]
        return gradeAbbrs[self.grade]

    def aftercareRoom(self):

        # Do we have an aftercare room assigned?
        if self.afcroom:
            return self.afcroom
        elif self.user.bears:
          return 'YES'

        return None


    def __repr__(self):
        return '<Student %r %r>' % (self.firstname, self.lastname)

    def __unicode__(self):
        return self.firstname + ' ' + self.lastname


# -----------------------------------------------------------
#  User (parent or admin)
# -----------------------------------------------------------
class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    firstname = db.Column(db.String(80))
    lastname = db.Column(db.String(80))
    phone = db.Column(db.String(80))
    passwd = db.Column(db.String(80))  # hashed password
    email = db.Column(db.String(120), unique=True)
    bears = db.Column(db.Boolean())

    # Scholarship info
    scholarship = db.Column(db.Boolean())
    scholgrant = db.Column(db.Integer())
    scholapply = db.Column(db.Integer())

    invoicesent = db.Column(db.DateTime(timezone=True))

    students = db.relationship("Student", backref="user")

    payments = db.relationship("Payment", backref="user")


    def displayname(self):
        return self.firstname + " " + self.lastname

    # Sort of hacky, just used to preview invoice in admin
    def invoiceReqsHtml(self):
        regReqsForStu, totalDue = mxinvoice.gatherInvoicedCoursesForUser(self)

        html = '<table class="table table-bordered">'
        html += "<tr><th>Class</th><th>Student</th><th>Fee</th>"
        for req in regReqsForStu:
            html += "<tr><td>" + req.course.titleGrade() +"</td>"
            html += "<td>" + req.student.displayname() + "</td>"
            if req.status=='enrolled':
                html += "<td>PAID</td>"
            else:
                html += "<td> $"+str(req.course.fee)+"</td><tr>\n"

        if (self.scholgrant):
            html += "<tr><td colspan='2'>Scholarship</td><td> $("+str(self.scholgrant)+")</td></tr>\n"

        html += "<tr><td colspan='2'>Total Due:</td><td><b>$"+str(totalDue)+"</b></td></tr>"
        html += "</table>"


        return html

    def fancyEmail(self):
        return "%s <%s>" % (self.displayname(), self.email)

    def totalEnrollDays(self):
        totalDays = 0
        for stu in self.students:
            if stu.enrollDays:
                totalDays += stu.enrollDays
        return totalDays

    def invoiceSentDesc(self):

        if self.invoicesent:
            return self.invoicesent.strftime('%a %b %d, %I:%M %p')
        else:
            return "Never"

    def __repr__(self):
        return '<User %r>' % self.email

    def __unicode__(self):
        return self.email


# -----------------------------------------------------------
#  Course Request
# -----------------------------------------------------------
class CourseRequest(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    student_id = db.Column(db.Integer, db.ForeignKey('student.id'))
    course_id = db.Column(db.Integer, db.ForeignKey('course.id'))

    # 1 = first choice, 2 = second choice, etc..
    priority = db.Column(db.Integer)

    # Requested - User signed up for the course
    # Provisional - Temporarily enrolled for scheduling, admin only
    # Registered - Assigned the course but not yet paid
    # Enrolled - Registered and paid
    status = db.Column(db.Enum("requested", "provisional",
                               "registered", "enrolled", "declined",
                               name='status_enum'), nullable=False)

    student = db.relationship(Student,
                              backref=db.backref("requests", order_by=priority))

    course = db.relationship(Course,
                             backref=db.backref("requests", order_by=status))

    def __repr__(self):
        return '<CourseRequest %d>' % self.id

    def __unicode__(self):

        priorityAbbr = {1: "1st", 2: "2nd"}

        return "%s %s %s" % (priorityAbbr.get(self.priority, str(self.priority)),
                             self.course.abbr(), self.status[:3])



    def second(self):
        stu = self.student
        course = self.course

        # Second choice logic is based on enroll days.
        if stu.enrollDays == 1:
            # For 1 enrollday, things are simple, since there should only be two CR
            for cr in stu.requests:
                if cr != self:
                    return cr

            # No alternate
            return None

        elif stu.enrollDays==3 :
            # For 3 enrolldays, same as for 1, but only look at
            # courses for today
            for cr in stu.requests:
                if cr != self and cr.course.weekday == course.weekday:
                    return cr

            # No alternate for this day
            return None

        # Last case, 2 enrolldays, is the tricky one.
        # Note that this logic will not work if we support more than 3 class days,
        # e.g. if we have classes more than just MWF...

        # first, count how many CR's they have for each day
        crForDay = {}
        totalCr = len(stu.requests)
        for day in WEEKDAYS:
            for cr in stu.requests:
                if cr.course.weekday == day:
                    crForDay[day] = crForDay.get( cr.course.weekday, 0 ) + 1

        # If they have an alternate on the same day, suggest that
        if crForDay[course.weekday] == 2:
            for cr in stu.requests:
                if cr != self and cr.course.weekday == course.weekday:
                    return cr
        else:
            # No alterate on the same day, look for a day with only an
            # alternate selected
            for cr in stu.requests:
                if cr.priority==2 and crForDay[cr.course.weekday]==1:
                    return cr

        # They didn't pick an alternate for this day, and they don't
        # have an alternate without a primary. No second choice.
        return None

# -----------------------------------------------------------
#  Payment
# -----------------------------------------------------------
class Payment(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    datetime = db.Column(db.DateTime(timezone=True))

    # info we get from stripe
    stripeTokenType = db.Column(db.String(20))
    stripeToken = db.Column(db.String(80))
    stripeEmail = db.Column(db.String(80))  # email they used to pay with

    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))
    amount = db.Column(db.Integer)


# -----------------------------------------------------------
#  Wipe and Rebuild DB
# -----------------------------------------------------------
def getBool( s ):
    if s.lower() in ['true', 'yes', '1']:
        return True
    else:
        return False

def rebuild_course_data( classDataPath ):
    print("POPULATE: generating course data...")

    # Get course data from json file
    course_fp = open( os.path.join( classDataPath, "courses.json") )
    coursedata = json.load(course_fp)
    course_fp.close()

    for course in coursedata["courses"]:
        c = Course()
        c.title = course["title"]
        c.weekday = course.get("weekday", "Monday")
        c.capacity = course.get("minimum", 6)
        c.capacity = course.get("capacity", 12)
        c.instructor = course.get("instructor", "TBA")
        c.instemail = course.get("instemail", "")
        c.picurl = course.get("picurl", "" )
        c.fee = course.get("fee", 0)
        c.featured = getBool( course.get("featured", "True") )
        c.mingrade = course.get("mingrade", 0 )
        c.maxgrade = course.get("maxgrade", 0 )

        if course.has_key("desc") and course["desc"]:
            print "desc is ", course["desc"]
            # c.desc = course["desc"]
            desc = codecs.open( os.path.join( classDataPath, course["desc"]), encoding='utf-8').read()
            unicodedata.normalize( 'NFKD', desc ).encode('ascii', 'ignore')
            c.desc = desc
        else:
            # No description text, use placeholder text
            r = requests.get("http://loripsum.net/api/plaintext/headers")
            c.desc = r.text

        if course.has_key("summary"):
            c.summary = course["summary"]
        else:
            c.summary = "No information available."

        db.session.add(c)
    db.session.commit()


def rebuild_db_OLD( classDataPath, populate):
    print "db is ", db

    db.drop_all()
    db.create_all()

    # Import the bare course data
    rebuild_course_data( classDataPath )

    # Populate with test data?
    if not populate:
        return

    fake = faker.Faker()

    # Populate with test users and students
    print("POPULATE: generating users and students...")
    for i in range(50):

        user = User()
        user.lastname = fake.last_name()
        user.firstname = fake.first_name()
        user.email = fake.email()
        user.passwd = genPasswd()

        if random.randint(0, 10) == 0:
            user.bears = True

        if random.randint(0, 10) == 0:
            user.scholarship = True

        db.session.add(user)

        # Make some students for this user
        numChilds = random.randint(0, 3)
        for c in range(numChilds):
            student = Student()
            student.lastname = user.lastname
            student.firstname = fake.first_name()
            student.grade = random.randint(0, 5)
            student.user = user
            student.enrollDays = random.randint(1, 3)
            db.session.add(student)

    db.session.commit()

    print("POPULATE: generating course requests...")
    courses = Course.query.all()

    days = ['Monday', 'Wednesday', 'Friday']

    # courseMon = Course.query.filter( Course.weekday=='Monday')
    # courseWed = Course.query.filter( Course.weekday=='Wednesday')
    # courseFri = Course.query.filter( Course.weekday=='Friday')

    # Now request a lot of courses
    students = Student.query.all()
    for stu in students:
        if stu.enrollDays == 1:
            # first, second from all week
            for p in [1, 2]:
                creq = CourseRequest()
                creq.student = stu
                creq.course = random.choice(courses)
                creq.priority = p
                creq.status = "requested"
                db.session.add(creq)
        else:
            if stu.enrollDays == 2:
                skipDay = random.choice(days)
            else:
                skipDay = 'Nope'

            for day in days:
                if day == skipDay:
                    continue

                courseDay = Course.query.filter(Course.weekday == day)

                # first, second from this day
                for p in [1, 2]:
                    creq = CourseRequest()
                    creq.student = stu
                    creq.course = random.choice(list(courseDay))
                    creq.priority = p
                    creq.status = "requested"
                    db.session.add(creq)

        db.session.commit()

    print("POPULATE: done...")






