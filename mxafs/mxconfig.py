import os, sys, string
import ConfigParser

config = ConfigParser.ConfigParser()

config.add_section('env')
config.set('env', 'name', 'Unknown')
config.set('env', 'db', 'sqlite:////tmp/mxafs.db')

config.add_section('auth')
config.set('auth', 'stripe-key', 'sk_test_4aFqb7RGBzAUPeSZxUxU47Qr')
config.set('auth', 'session-secret', '123456789')


def readConfig(cfgfilename):
    config.read(cfgfilename)


def writeConfig(cfgfilename):
    with open(cfgfilename, 'wt') as configfp:
        config.write(configfp)




