#!/bin/env python
# -*- coding: utf-8 -*-

import os, sys, string
import random

from flask import request, session, redirect, url_for, Markup, Response, abort

from flask.ext.admin import Admin, BaseView, expose
from flask.ext.admin.contrib.sqla import ModelView
from flask.ext.admin.model.template import macro

from wtforms.fields import SelectField

from mxafs.model import db, WEEKDAYS, course_students
from mxafs.model import User, Student, Instructor, CourseInfo, Course, CourseRequest, Payment, Classroom
from mxafs.model import CalendarEvent, calendarDates
from mxafs.model import Settings

from mxafs import mxinvoice, mxemail

from flask.ext.admin import Admin, expose, AdminIndexView

from sqlalchemy import or_, and_

import markdown
import datetime

SECRET_ADMIN_TOKEN = 'Y5<84jM>v$:[sU}'

def compareUsersId( userA, userB ):

        return userA.id - userB.id

def checkAuth():
    return session.get('admin_auth', None) == SECRET_ADMIN_TOKEN

class HomeView(AdminIndexView):

    @expose("/")
    def index(self):

        if not checkAuth():
            return self.render('admin/login.html')
        else:
            return self.render('admin/index.html')

    @expose("/admlogin", methods=['POST'])
    def admlogin(self):

        if request.form['password'] == 'together600':
            session['admin_auth'] = SECRET_ADMIN_TOKEN
        else:
            session['admin_auth'] = None

        return redirect(url_for('.index'))

    @expose("/admlogout" )
    def admlogout(self):
        session['admin_auth'] = None

        return redirect(url_for('.index'))

    @expose( "/payment/<int:userId>", methods=["GET", "POST"])
    def payment(self, userId):

        if not checkAuth():
            return redirect( url_for('.index'))

        user = User.query.get( userId )
        if not user:
            return "<h1>Error: no such user: %d</h1>" % userId

        # Get reg requests
        regReqsForStu, totalDue = mxinvoice.gatherInvoicedCoursesForUser(user)

        for k, v in request.form.iteritems():
            print "KV", k, v

        # Did they hit the "apply payment" button?
        if request.form.has_key('btn-payment'):

            memo = request.form.get( 'memo', "No Info")

            # Make a dummy stripeInfo
            stripeInfo = ( 'admin', memo, user.email )
            mxinvoice.processPayment( user, totalDue, stripeInfo, regReqsForStu )

            # redirect back to invoice page
            return redirect( url_for('.invoice'))

        return self.render( 'adm_payment.html', user=user, reqs=regReqsForStu, total=totalDue )

    @expose("/enroll-ewp/<int:userId>", methods=["GET", "POST"])
    def enrollWithoutPayment(self, userId):

        if not checkAuth():
            return redirect( url_for('.index'))

        user = User.query.get( userId )
        if not user:
            return "<h1>Error: no such user: %d</h1>" % userId

        # Get reg requests
        regReqsForStu, totalDue = mxinvoice.gatherInvoicedCoursesForUser(user)

        mxinvoice.enrollWithoutPayment( regReqsForStu )

        # redirect back to invoice page
        return redirect( url_for('.invoice'))




    @expose("/invoice", methods=["GET", "POST"])
    def invoice(self):

        if not checkAuth():
            return redirect( url_for('.index'))

        message = None
        messageclass = "info"

        # If this is a result of the invoice button being pressed
        if request.form.has_key('btn-invoice'):
            total = 0
            succeed = 0
            for k, v in request.form.iteritems():
                if (k.startswith("invoice-")) and (v=='on'):
                    userId = int(k.split('-')[-1])
                    user = User.query.get( userId )
                    total += 1
                    if self.sendInvoiceForUser( user ):
                        succeed += 1

            if (succeed < total):
                message = "<strong>Some emails failed to send.</strong> Emails were sent to %d out of %d users." % (succeed, total)
                messageclass ="error"
            else:
                message = "<strong>Emails Sent!</strong> Emails sent successfully to %d users." % (succeed)
                messageclass = "success"

        # Get all course requests in "registered" state
        invoiceCR = CourseRequest.query.filter( CourseRequest.status == 'registered')
        invoiceUsers = set()
        invoiceData = {}


        for cr in invoiceCR:
            user = cr.student.user
            invoiceUsers.add(user)

            # Start with credit if they have scholarship grant
            startBal = 0
            if user.scholgrant:
                startBal = -user.scholgrant

            balance, count = invoiceData.get( user.id, ( startBal, 0) )
            invoiceData[ user.id ] = ( balance + cr.course.fee, count+1 )

        invoiceUsers = list(invoiceUsers)
        invoiceUsers.sort(cmp=compareUsersId )

        return self.render('adm_invoice.html', users=invoiceUsers, data=invoiceData,
                           message=message, messageclass=messageclass )

    @expose('/userinfo/<int:userId>')
    def userInfo(self, userId):

        user = User.query.get(userId)

        return self.render('adm_user_info.html', user=user  )

    @expose("/report_waitlist")
    def reportWaitlist(self):

        if not checkAuth():
            return redirect( url_for('.index'))

        reqs = CourseRequest.query.filter( CourseRequest.seconded==True ).order_by( CourseRequest.altcourse_id )

        return self.render('reports/rep_waitlist.html', reqs=reqs )


    @expose("/report_afc_class.tsv")
    def reportAfcClass(self):

        if not checkAuth():
            return redirect( url_for('.index'))

        tsvlines = ""

        students = Student.query.all()
        students.sort( key=lambda s: ( s.displayname(),  s.learns, s.bears ) )
        for stu in students:
            if stu.learns or stu.bears:
                if stu.learns:
                    afc = 'LEARNS'
                elif stu.bears:
                    afc = 'BEARS'

                for course in stu.courses:
                    data = ( stu.displayname(), afc, course.weekday, course.titleGrade(), course.room )
                    tsvlines += string.join( data, "\t" ) + "\n"

        return Response( tsvlines, mimetype='text/tsv')



    @expose("/report_rosters")
    def reportRosters(self):

        if not checkAuth():
            return redirect( url_for('.index'))

        courses = Course.query.all()

        return self.render('reports/rep_roster.html', courses=courses)

    @expose("/report_roster/<course_id>")
    def reportRoster(self, course_id):

        # NOTE: These are not auth'd so teachers can access

        course = Course.query.get( course_id )
        if not course:
            abort(404)

        # For individual rosters, also include class email list
        users = set()
        for stu in course.students:
            users.add( stu.user )

        # Special case: add the administrator to all class email lists
        adminUser = User.query.filter( User.email == 'vczdavis@gmail.com').first()
        if adminUser:
            users.add( adminUser )

        # Format and sort emails
        userEmails = map( lambda u: '%s <%s>' % (u.displayname(), u.email), users )
        userEmails.sort()

        return self.render('reports/rep_roster.html', courses=[ course ], classEmail=userEmails )


    @expose("/report_roster_csv/<course_id>")
    def reportRosterCSV(self, course_id ):

        # NOTE: These are not auth'd so teachers can access

        # Remove course prefix , eg 6-ceramics.csv
        course_id = string.split( course_id, '-' )[0]

        course = Course.query.get( course_id )
        if not course:
            abort(404)


        results = ""
        fieldTitles = ['Student','Grade','Teacher','Parent','Phone', 'Email', 'LEARNS/BEARS'];

        results += ','.join( fieldTitles ) + '\n'

        for stu in course.students:

            if stu.bears:
                bears = "BEARS"
            elif stu.learns:
                bears = "Learns"
            else:
                bears = "no"


            fields = [ stu.displayname(), stu.gradeabbr(),
                       str(stu.teacher), stu.user.displayname(),
                       str(stu.user.phone), str(stu.user.email),
                       bears ]

            results += ','.join( fields ) + '\n'

        return Response( results, mimetype='text/csv')


    @expose("report_afcroom")
    def reportAftercare(self):

        studentsWithAftercare = []
        for stu in Student.query.all():
            if stu.aftercareRoom() or stu.learns or stu.bears:
                studentsWithAftercare.append( stu )

        print studentsWithAftercare
        studentsWithAftercare.sort( key=lambda s: ( s.learns, s.bears, s.displayname()) )

        return self.render( 'reports/rep_afcroom.html',
                            students=studentsWithAftercare )

    @expose("/report_scholarships")
    def reportScholarships(self):

        schol_grant = 0
        schol_grant_count = 0
        schol_applied = 0
        schol_applied_count = 0
        #enrolled_total = 0
        #registered_total = 0


        for u in User.query.all():
            if u.scholgrant:
                schol_grant += u.scholgrant
                schol_grant_count += 1
            if u.scholapply:
                schol_applied += u.scholapply
                schol_applied_count += 1

        return self.render('reports/rep_schol2.html',
                           schol_grant=schol_grant,
                           schol_grant_count=schol_grant_count,
                           schol_applied=schol_applied,
                           schol_applied_count=schol_applied_count )

    @expose("/report_payment")
    def reportPayment(self):

        if not checkAuth():
            return redirect( url_for('.index'))

        schol_grant = 0
        schol_applied = 0
        enrolled_total = 0
        registered_total = 0


        for u in User.query.all():
            if u.scholgrant:
                schol_grant += u.scholgrant
            if u.scholapply:
                schol_applied += u.scholapply

        for cr in CourseRequest.query.filter( or_( CourseRequest.status=='registered',
                                                    CourseRequest.status=='enrolled') ):
            if cr.status == 'registered':
                registered_total += cr.course.fee
            elif cr.status == 'enrolled':
                enrolled_total += cr.course.fee

        payment_total = 0
        manual_total = 0
        for p in Payment.query.all():
            payment_total += p.amount
            if p.stripeTokenType == 'admin':
                manual_total += p.amount


        return self.render('reports/rep_schol.html',
                           payment_total=payment_total,
                           manual_total=manual_total,
                           enrolled_total=enrolled_total,
                           registered_total=registered_total,
                           schol_grant=schol_grant,
                           schol_given=schol_applied,
                           schol_total=schol_grant + schol_applied )

    @expose("/report_teachers")
    def reportTeachers(self):

        print "In report teachers"
        # Get all the Classrooms (school day teachers)
        teachers = Classroom.query.order_by( Classroom.grade, Classroom.instname )

        return self.render('reports/rep_teacher.html',
                           teachers=teachers )

    @expose("/report_learns")
    def reportLearns(self):

        print "In Report LEARNs"
        gradeAbbr = { 0:'TK/K', 1:'1st', 2:'2nd', 3:'3rd', 4:'4th', 5:'5th'}
        stusByGrade = { }

        stus = Student.query.filter( Student.learns == True )
        for stu in stus:
            if not stusByGrade.has_key(stu.grade):
                stusByGrade[stu.grade] = []

            stusByGrade[stu.grade].append( stu )

        for sk in stusByGrade.keys():
            stusByGrade[sk].sort(key=lambda s: s.firstname )

        return self.render( 'reports/rep_learns.html',
                            gradeAbbr=gradeAbbr,
                            stusByGrade = stusByGrade )

    @expose("/report_bears")
    def reportBears(self):

        print "In Report BEARSs"
        gradeAbbr = { 0:'TK/K', 1:'1st', 2:'2nd', 3:'3rd', 4:'4th', 5:'5th'}
        stusByGrade = { }

        stus = Student.query.filter( Student.bears == True )
        for stu in stus:
            if not stusByGrade.has_key(stu.grade):
                stusByGrade[stu.grade] = []

            stusByGrade[stu.grade].append( stu )

        for sk in stusByGrade.keys():
            stusByGrade[sk].sort(key=lambda s: s.firstname )

        return self.render( 'reports/rep_learns.html',
                            gradeAbbr=gradeAbbr,
                            stusByGrade = stusByGrade )

    # TODO: maybe one day make these a Reporting view
    @expose("/report_course/<int:mingrade>/<int:maxgrade>" )
    def reportCourseDescs(self, mingrade, maxgrade):

        if not checkAuth():
            return redirect( url_for('.index'))

        courseList = Course.query.filter( and_( Course.maxgrade >= mingrade,
                                                Course.mingrade <= maxgrade ) )

        courseDescs = {}
        for course in courseList:
            cinfo = course.course_info
            if cinfo.desc:
                desc = Markup(markdown.markdown(cinfo.desc))
            else:
                desc = "No course description available."

            courseDescs[ cinfo.slug ] = desc

        grid = {}
        maxGrid = 0
        days = ['Monday', 'Wednesday', 'Friday']
        for weekday in days:
            grid[weekday] = []
            for cs in courseList:
                if cs.weekday == weekday:
                    grid[weekday].append( cs )
                    if len(grid[weekday]) > maxGrid:
                        maxGrid = len(grid[weekday])

        return self.render('reports/rep_courses.html',
                           weekdays = days,
                           grid=grid, maxGrid=maxGrid,
                           courses=courseList,
                           descs=courseDescs )

    @expose("report_emails")
    def reportAllEmails(self):

        emails = []
        for user in User.query.all():
            email = user.fancyEmail()
            email = email.replace( "<", "&lt" )
            email = email.replace( ">", "&gt" )
            emails.append( email )

        emails.sort()

        result = ""
        batch_size = 80
        for i in range(0, len(emails), batch_size):
            result += ", ".join( emails[i:i+batch_size] )

            result += "<hr>"

        return result

    @expose("clear_users")
    def clearUsers(self):

        result = ""
        resetEnabled = os.environ.get('MXASE_ALLOW_RESET', 0 )
        if not resetEnabled:
            result = "Reset functions not enabled"
            return result

        # Clear the waitlist flag on all courses
        for course in Course.query.all():
            course.waitlist = False
        db.session.commit()

        # Remove students from enrolled courses (even if we delete students
        db.session.execute( course_students.delete() )
        db.session.commit()

        # User, Student, Instructor, CourseInfo, Course, CourseRequest, Payment, Classroom
        numCrDeleted = CourseRequest.query.delete()
        result += "Deleted %d Course Requests<br>" % (numCrDeleted)

        numStudentDeleted = Student.query.delete()
        result += "Deleted %d Students<br>" % (numStudentDeleted)

        numUserDeleted = User.query.delete()
        result += "Deleted %d Users<br>" % (numUserDeleted)

        numPaymentDeleted = Payment.query.delete()
        result += "Deleted %d Payments<br>" % (numPaymentDeleted)



        db.session.commit()

        return result

    def sendInvoiceForUser( self, user ):

        # Get a list of registered courses
        regReqsForStu, totalDue = \
            mxinvoice.gatherInvoicedCoursesForUser(user)

        # Send the email
        if mxemail.sendInvoiceEmail(user, regReqsForStu, totalDue, calendarDates() ):
            # If email was sent, record the time
            user.invoicesent = datetime.datetime.now()
            db.session.commit()

            return True
        else:
            return False

class ScheduleAdminView(BaseView):

    def is_accessible(self):
        return checkAuth()


    @expose('/')
    def index(self):

        # Flag Reqs missing a student
        badreqs = list(CourseRequest.query.filter( CourseRequest.student == None ))


        return self.render('adm_sched_list.html',
                           badreqs = badreqs,
                           courses=Course.query.all())

    @expose('/info/<int:stuId>')
    def stuInfo(self, stuId):

        student = Student.query.get(stuId)
        user = student.user
        weekdays = [ 'Monday', 'Wednesday', 'Friday'] # TODO: get from reqs
        return self.render('adm_stu_info.html', student=student, user=user, weekdays=weekdays )

    @expose('/course/<int:courseId>', methods=['GET', 'POST'])
    def course(self, courseId):

        course = Course.query.get(courseId)
        if not course:
            abort(404)

        # Are there button actions requested?
        if (request.form.has_key('btn-enroll')):
            # Provisionally register in their first choice
            self.enrollRequest(int(request.form['btn-enroll']))
        elif (request.form.has_key('btn-second')):
            # Provisionally register in their second choice
            self.secondRequest(int(request.form['btn-second']))
        elif (request.form.has_key('btn-waitlist')):
            # Waitlist the student
            self.waitlistRequest(int(request.form['btn-waitlist']))
        elif (request.form.has_key('btn-register')):
            # Confirm a provisional registration
            self.registerRequest(int(request.form['btn-register']))
        elif (request.form.has_key('btn-request')):
            # Set status back to requested
            self.revertRequest(int(request.form['btn-request']))
        elif (request.form.has_key('btn-drop')):
            # Drop student (e.g. they changed their mind, or waitlist closed)
            self.dropRequest(int(request.form['btn-drop']))
        elif (request.form.has_key('btn-unregister')):
            # Remove registration back to provisional
            self.unregisterRequest(int(request.form['btn-unregister']))

        courses = Course.query.all()

        # Get the requests for this course
        reqs = course.requests

        # It's a pain to filter this in the query, and especially
        # to get them to sort by status (non-alphabeticaly). Since there's
        # only a handful of these, just do it in python

        statusOrder = {'enrolled': 0,
                       'registered': 1,
                       'provisional': 2,
                       'requested': 3,
                       'waitlist' : 4,
                       'dropped': 5 }


        reqs.sort(key=lambda r: (statusOrder[r.status] * 1000) + (r.waitlist or 0) )

        # Reqs missing a student
        badreqs = list(CourseRequest.query.filter(
            and_( CourseRequest.course == course,
                  CourseRequest.student == None )) )

        return self.render('adm_sched_course.html',
                           badreqs = badreqs,
                           course=course, courses=courses, requests=reqs)

    @expose('/lotto/<int:courseId>/<int:count>', methods=['GET'])
    def lotto(self, courseId, count):

        course = Course.query.get(courseId)
        if not course:
            abort(404)

        # Randomly enroll one requested student
        reqs = list(CourseRequest.query.filter( and_(CourseRequest.course == course,
                                                     CourseRequest.status=='requested')) )

        # Limit count to class capacity
        maxCount = course.capacity
        for cr in course.requests:

            # Count slots already used
            if (cr.status in ['provisional', 'registered', 'enrolled']):
                maxCount -= 1

        if maxCount < count:
            count = maxCount

        print "Max count ", maxCount
        while (reqs and count):

            # Choose one at random
            luckyReq = random.choice( reqs )

            #print count, "will enroll ", luckyReq, luckyReq.student.displayname(), luckyReq.course.titleGrade()
            luckyReq.status = "provisional"
            db.session.commit()


            # Remove the enrolled req
            reqs.remove( luckyReq )

            # Decrement count
            count = count - 1

        # Done with lotto, redirect back to same page to prevent re-submits
        return redirect(url_for('schedule.course', courseId=course.id))

    def enrollRequest(self, reqId):
        req = CourseRequest.query.get(reqId)
        if (req.status in [ "requested", "waitlist" ]):
            req.status = "provisional"

            db.session.commit()

    def waitlistRequest(self, reqId):
        req = CourseRequest.query.get(reqId)

        # get the max waitlisted request for this class
        waitlistNum = 0
        for cr in CourseRequest.query.filter( CourseRequest.course == req.course ):
            if (cr == req):
                continue

            if cr.waitlist > waitlistNum:
                waitlistNum = cr.waitlist

        if (req.status == "requested"):
            req.status = 'waitlist'
            req.waitlist = waitlistNum + 1

            db.session.commit()

    def dropRequest(self, reqId ):

        req = CourseRequest.query.get(reqId)
        if (req.status != 'enrolled'):
            req.status = 'dropped'

            db.session.commit()

    def secondRequest(self, reqId):
        req = CourseRequest.query.get(reqId)

        # This only makes sense if they have an alternate course
        if not req.altcourse:
            return

        # Decline the first choice by swap the first and second
        # choices, then set status to registerd
        oldfirstChoice = req.course
        req.course = req.altcourse
        req.altcourse = oldfirstChoice
        req.seconded = True
        req.status = "registered"

        db.session.commit()

    def unregisterRequest(self, reqId):
        req = CourseRequest.query.get(reqId)
        if (req.status == "registered"):
            req.status = "provisional"

            db.session.commit()


    def registerRequest(self, reqId):
        req = CourseRequest.query.get(reqId)
        if (req.status == "provisional"):
            req.status = "registered"

            db.session.commit()

    def revertRequest(self, reqId):
        req = CourseRequest.query.get(reqId)
        req.status = "requested"
        db.session.commit()


# Override displayed fields
class CourseModelView(ModelView):
    page_size = 50
    column_list = ( 'course_info', 'mingrade', 'maxgrade', 'weekday', 'instructor', 'waitlist', 'roster'  )
    list_template = 'admin/model/course_list.html'

    column_formatters = dict( roster=macro('course_roster') )

    def is_accessible(self):
        return checkAuth()

class CourseInfoModelView(ModelView):

    page_size = 50
    column_list = ( 'title', 'slug', 'summary' )

    def is_accessible(self):
        return checkAuth()


class CourseRequestModelView(ModelView):
    page_size = 100

    def is_accessible(self):
        return checkAuth()

class StudentView(ModelView):

    column_list = ( 'firstname', 'lastname', 'grade', 'bears', 'learns', 'afcroom', 'user', 'classroom' )

    def is_accessible(self):
        return checkAuth()


# Override displayed fields
class PaymentModelView(ModelView):
    column_list = ( 'user', 'stripeTokenType', 'stripeToken', 'datetime', 'amount' )

    def is_accessible(self):
        return checkAuth()

class InstructorModelView(ModelView):
    page_size = 50
    column_list = ( 'instname', 'email', 'phone', 'rate' )

    def is_accessible(self):
        return checkAuth()


class UserModelView(ModelView):
    column_list = ( 'firstname', 'lastname', 'passwd', 'email', 'bears' )
    list_template = 'admin/model/user_list.html'
    edit_template = 'admin/model/user_edit.html'
    column_formatters = dict(bears=macro('user_info'),
                             passwd=macro('user_passwd'))

    def is_accessible(self):
        return checkAuth()

class ClassroomModelView(ModelView):

    def is_accessible(self):
        return checkAuth()

class SettingsModelView(ModelView):

    def is_accessible(self):
        return checkAuth()

class CalendarEventModelView(ModelView):
    page_size = 100
    def is_accessible(self):
        return checkAuth()


# Admin interface
def init_admin(app):

    #db.create_all()

    # admin = Admin(app)
    admin = Admin(app, "ase.mxpta.org", index_view=HomeView(name='Home'))
    admin.add_view(ScheduleAdminView(name='Schedule', endpoint='schedule'))

    admin.add_view(UserModelView(User, db.session, category='Users'))
    admin.add_view(StudentView(Student, db.session, category='Users'))

    # TODO: Make auth'd model view for CourseInfo and Instructor
    admin.add_view(CourseInfoModelView(CourseInfo, db.session, category='Catalog'))
    admin.add_view(CourseModelView(Course, db.session, category='Catalog'))
    admin.add_view(CourseRequestModelView(CourseRequest, db.session, category='Catalog'))
    admin.add_view(InstructorModelView(Instructor, db.session, category='Catalog'))

    admin.add_view(ClassroomModelView( Classroom, db.session, category='Misc'))
    admin.add_view(PaymentModelView(Payment, db.session, category='Misc'))
    admin.add_view(CalendarEventModelView( CalendarEvent, db.session, category='Misc'))
    admin.add_view(SettingsModelView( Settings, db.session, category='Misc'))
