# Query script for people that were waitlisted int the old version

import os, sys, string
import datetime

from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from sqlalchemy import or_, and_

from mxafs import model
#from mxafs import model_old

if __name__=='__main__':

    app = Flask(__name__)
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////Users/joeld/Projects/mx-afterschool/db/mxase.sqlite_AUG29_.BAK'
    model_old.db.app = app
    model_old.db.init_app(app)


    crs = model_old.CourseRequest.query.filter(
        and_ (model_old.CourseRequest.status == 'declined',
              model_old.CourseRequest.priority == 1 ) )


    for cr in crs:
        user = cr.student.user

        second = cr.second()
        if not second:
            second = "None"
        else:
            second = second.course.titleGrade()

        # Get what the student was actually enrolled in
        stu = cr.student

        stuEnr = []
        for scr in stu.requests:
            if scr.status=='enrolled':
                stuEnr.append(scr)

        if (len(stuEnr) == 1):
            # Only enrolled in 1 course
            enr = stuEnr[0]
        else:
            # was enrolled in more than one course, find the one
            # on the same day as their first choice
            for scr in stuEnr:
                #print scr.course.weekday, scr.course.titleGrade(), scr.priority, scr.status
                if scr.course.weekday == cr.course.weekday:
                    enr = scr
                    break
        #print "ENR: ", enr.course.titleGrade()

        data = ( cr.course.titleGrade(), user.displayname(), user.email, cr.student.displayname(), second, enr.course.titleGrade() )
        print "\t".join(data)


