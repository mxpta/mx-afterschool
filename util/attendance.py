#!/bin/env python
# -*- coding: utf-8 -*-
import os, sys, string
import datetime

import fpdf
import unicodedata

print sys.path
sys.path.append( '/Users/joeld/Projects/mx-afterschool' )
from mxafs import model

from flask import Flask
from flask import escape
from flask.ext.sqlalchemy import SQLAlchemy


app = Flask(__name__)
app.config.from_envvar( 'MXPTA_SETTINGS' )

model.db.app = app
model.db.init_app(app)


CLASS_DAYS = {}

def fetchHolidays():
    holidays = []

    events = model.CalendarEvent.query.all()
    for ev in events:
        if ev.role == 'Holiday':
            print "Holiday: ", ev.title, ev.date
            holidays.append( ev.date )

    return holidays

def initClassDaysForWeekday( weekday, startDate ):

    curr = startDate
    CLASS_DAYS[ weekday ] = []

    holidays = fetchHolidays()

    lastDay = model.CalendarEvent.query.filter( model.CalendarEvent.role == 'LastDay').first()
    while curr <= lastDay.date:

        if not curr in holidays:
            CLASS_DAYS[weekday].append(curr)

        curr = curr + datetime.timedelta( days=7 )

    # print "Class days for ", weekday
    # for d in CLASS_DAYS[weekday]:
    #     print d.strftime( "%a, %b. %d")


def initClassDays():

    firstDay = model.CalendarEvent.query.filter( model.CalendarEvent.role == 'FirstDay').first()

    print "First day of class ", firstDay.date, firstDay.date.weekday()
    firstMonday = None
    firstWednesday = None
    firstFriday = None

    curr = firstDay.date
    while (not firstMonday) or (not firstWednesday) or (not firstFriday):
        weekday = curr.weekday()
        if (not firstMonday) and (weekday==0):
            firstMonday = curr
        elif (not firstWednesday) and (weekday==2):
            firstWednesday = curr
        elif (not firstFriday) and (weekday==4):
            firstFriday = curr

        curr = curr + datetime.timedelta( days=1 )
        print curr, curr.weekday()

    print "First monday", firstMonday
    print "First wednesday", firstWednesday
    print "First friday", firstFriday

    initClassDaysForWeekday( 'Monday', firstMonday )
    initClassDaysForWeekday( 'Wednesday', firstWednesday)
    initClassDaysForWeekday( 'Friday', firstFriday)

def makeAttendanceSheet( course, allPdf=None ):
    
    if allPdf:
        pdf = allPdf
    else:
        pdf = fpdf.FPDF()
        pdf.set_auto_page_break( False, 25*0.25 )

    all_days = CLASS_DAYS[ course.weekday ]

    stubears = set()
    stulearns = set()
    stunames = []

    for stu in course.students:
        stunames.append( stu.displayname() )

        if stu.bears:
            stubears.add(stu.displayname() )

        if stu.learns:
            stulearns.add(stu.displayname() )            

    stunames.sort()

    # HACK: figure out a better way than hardcoding these
    groupsForDay = {'Monday' : [ 4, 4, 4, 10],
                    'Wednesday' : [ 4, 4, 4, 10],
                    'Friday' : [ 4, 4, 4, 10] }

    startDay = 0
    for p in groupsForDay[course.weekday]:
        print "==="
        pagedays = all_days[startDay:startDay+p]
        startDay += p

        if len(pagedays)==0:
            print "No pagedays..."
            continue

        monthStart = pagedays[0].strftime("%B")
        monthEnd = pagedays[-1].strftime("%B")

        # Hack just to print tomorrows classes
        # if not monthStart == 'September':
        #     continue


        if monthEnd==monthStart:
            dateRange = monthStart
        else:
            dateRange = "%s/%s" % (monthStart, monthEnd)


        pdf.add_page( 'L')
        pdf.set_font('Arial', 'B', 24)

        # Class header
        pdf.cell( 40, 10, "Attendance -- %s %s" % ( course.titleGrade(), dateRange) )
        pdf.ln()

        CELLW = 200 / len(pagedays)
        CELLH = 8

        # Column headers
        pdf.set_font('Arial', 'B', 12)
        pdf.set_draw_color( 0,0,0 )
        pdf.set_line_width( 0.5 )
        pdf.cell( 70,10, 'Student', 'B' )
        #pdf.cell( 70,10, course.weekday, 'B' )

        for d in pagedays:
            print d.strftime("%a, %b %d")
            pdf.cell( CELLW,10, d.strftime("%a, %b %d"), 'B' )

        # Students
        pdf.set_xy( 10, 30)
        pdf.set_font('Arial', '', 12)
        pdf.set_line_width( 0.25 )
        count = 0
        needPageBreak = False
        if len(stunames) > 21:
            needPageBreak = True

        for name in stunames:

            name = unicodedata.normalize('NFKD', name).encode('ascii','ignore')

            if (needPageBreak):
                celltext = ""
                if name in stubears:
                    celltext = " (BEARS)"
                elif name in stulearns:
                    celltext = " (LEARNS)"
                name = name + celltext

            print name
            pdf.cell( 70,CELLH, name, True )

            for d in pagedays:
                pdf.cell( CELLW,CELLH, "", True )

            # Hacky page break
            count += 1
            if count > 21:
                count = 0
                pdf.add_page( 'L')

            pdf.ln()

        # BEARS
        if not needPageBreak:
            # FIXME HACK : if we have to do a page break, that messes
            # up our learns/bears labels. For now just don't add
            # those labels if this happens
            pdf.set_xy( 10, 30)
            pdf.set_font('Arial', '', 8)
            for name in stunames:
                celltext = ""
                if name in stubears:
                    celltext = "BEARS"
                elif name in stulearns:
                    celltext = "LEARNS"

                pdf.cell( 70,CELLH, celltext, align='R')
                pdf.ln()


    if not allPdf:

        # remove slashes from TK/K
        courseTitle = course.titleGrade();
        courseTitle = courseTitle.replace( "/K", "")

        pdf.output( os.path.expanduser(( os.path.join ("~", "attend", 
            courseTitle + ' Attendance.pdf') )))

if __name__=='__main__':

    initClassDays()

    # Two passes, one to make a master pdf with all classes, one
    # to make per-class pdfs
    for p in range(2):
        if p == 0:
            pdf = fpdf.FPDF()
            pdf.set_auto_page_break( False, 25*0.25 )
        else:
            pdf = None

        for c in model.Course.query.all():            
            print c.titleGrade(), c.weekday

            # Skip empty classes
            print len(list(c.students)), "students..."
            if len(list(c.students))==0:
                print "Class empty"
                continue

            makeAttendanceSheet( c, pdf )

        if p==0:
            pdf.output( os.path.expanduser(( os.path.join ("~", "attend", "mxase-attendance.pdf") )))


    # pdf = fpdf.FPDF()
    # pdf.add_page( 'L')
    # pdf.set_font('Arial', 'B', 16)
    # pdf.cell( 40, 10, 'Hello World')
    # pdf.output( 'attendance.pdf')