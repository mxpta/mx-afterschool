
import os, sys, string
import datetime

from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from sqlalchemy import or_, and_


from mxafs import model
from mxafs import model_old

# Hacky script to migrate the data from the fall 2014 DB into the newer
# spring 2015 database

def migrateData():

    # Old DB
    app = Flask(__name__+"OLD")
    app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////Users/joeld/Projects/mx-afterschool/db/mxase.sqlite_BAK_DEC27_LOCAL'
    model_old.db.app = app
    model_old.db.init_app(app)

    # New DB
    appNew = Flask(__name__)
    appNew.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////Users/joeld/Projects/mx-afterschool/db/mxase.sqlite'
    model.db.app = appNew
    model.db.init_app(appNew)

    model.db.create_all()

    # inst = model.Instructor()
    # inst.email = "test@example.com"
    # inst.instname = "Test Inst"
    # model.db.session.add(inst)
    model.db.session.commit()

    print "Migrate Courses and Instructors:"
    for c in model_old.Course.query.all():

        # Create a new course desc if we need to
        print c.title, c.slug

        cslug = c.slug

        #merge the "-k" courses with the regular ones
        if (cslug.endswith("-k")):
            cslug = cslug[:-2]

        cinfo = model.CourseInfo.query.filter( model.CourseInfo.slug == cslug )
        cinfo = list(cinfo)


        if not cinfo:
            print "Didn't find course_info for ", c.slug, "creating..."
            cinfo = model.CourseInfo()
            cinfo.title = c.title
            cinfo.slug = cslug
            cinfo.summary = c.summary
            cinfo.desc = c.desc
            cinfo.picurl = c.picurl

            model.db.session.add(cinfo)
            model.db.session.commit()

        else:
            print "found course info ", c.slug
            cinfo = cinfo[0]

        print c, c.title

        nc = model.Course()
        nc.classgroup = c.classgroup
        nc.weekday = c.weekday
        nc.starttime = c.starttime
        nc.duration = c.duration
        nc.minimum = c.minimum
        nc.capacity = c.capacity
        nc.fee = c.fee
        nc.waitlist = c.waitlist
        nc.mingrade = c.mingrade
        nc.maxgrade = c.maxgrade
        nc.course_info = cinfo

        model.db.session.add(nc)
        model.db.session.commit()


        # Create a new instructor if we haven't yet
        inst = model.Instructor.query.filter( model.Instructor.email == c.instemail )
        inst = list(inst)

        if not inst:
            inst = model.Instructor()
            inst.email = c.instemail
            inst.instname = c.instructor
            inst.bio = c.instbio

            model.db.session.add(inst)
            model.db.session.commit()
            nc.instructor = inst
        else:
            print "already added inst ", c.instemail
            nc.instructor = inst[0]

        model.db.session.commit()




    #for i in model.Instructor.query.all():
    #    print "Inst:", i


if __name__ == '__main__':

    migrateData()
