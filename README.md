Description
--------------------
This is a web-app to manage afterschool classes offered by the Malcolm X Elementary
School in Berkeley, CA. 

Questions or comments, ask Joel Davis (joeld42@gmail.com)

Setup Instructions:
--------------------
1) Install virtualenv and Flask, set up virtualenv. Instructions:
http://flask.pocoo.org/docs/0.10/installation/

2) With your virtualenv active, install requirements: 

  $ pip install -r requirements.txt 

(or install the requirements listed in requirements.txt
manually if you want. I don't think  there's too much version specific stuff
in there)

3) set the MXPTA_SETTINGS environment variable to point to where you cfg file is.

My dev cfg file is in cfg/mxpta-dev.cfg for example. Note: only test keys for stripe
and stuff are in here, don't check in any production keys into git, ever.

4) To initialize an empty database, uncomment the "rebuild_db2" lines at the
bottom of mx_afterschool.py, and run the script, then recomment them. You may
need to create a db directory.

5) To run development version, run:

  $ python mx_afterschool.py

6) To deploy a production version, do whatever wsgi hosting stuff your hosting requires. 

If you want to be visible beyond localhost (e.g. for testing on a mobile device) set
your local ip in mx_afterschool.py at the bottom in app.run(...)


Notes:

- The password for the admin interface can be found in admin.py, in
  admlogin(). This is not a real serious password, it's just to keep out
  curious people who type /admin.. if you have   stronger security requirements
  for your PTA community, I reccomend using apache mod_auth to authenticate the
  /admin path. (I'm not using it for MX because our host has an old version of
  apache that doesn't support auth on url patterns.)

- If you are working in the "mx-pta/mxafterschool" project on BitBucket,
  please create a branch for your school in git.

- I use "PyCharm CE" from https://www.jetbrains.com/pycharm/ to edit the project, 
  but any python-aware editor will work.

- There is a some code in there to generate "placeholder" people, and to import classes from
  json files for testing. This is all out-of-date now and won't work without some tinkering.

  