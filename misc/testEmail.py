import os, sys, string

import smtplib

from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

# tests
TEST = {
    'fake' : ( '192.168.1.111', 2525, None, None ),
    'dh'  : ( 'mail.ase.mxpta.org', 587, 'afterschool@ase.mxpta.org', 'MMrHT2rT' )	
}

if __name__=='__main__':
    if (len(sys.argv) < 1) or (not TEST.has_key(sys.argv[1]) ):
        print "Usage: %s <testname>" % (sys.argv[0], )
        sys.exit(1)

    server, port, user, passwd = TEST[sys.argv[1]]

    fromAddr = 'MX PTA Afterschool <afterschool@mxpta.org>'
    toAddr = 'Joel Davis <joeld42@gmail.com>'

    # Create message container - the correct MIME type is multipart/alternative.
    msg = MIMEMultipart('alternative')
    msg['Subject'] = 'Test Email'
    msg['To'] = toAddr

    # Create the body of the message (a plain-text and an HTML version).
    text = "This is a test email."
    html = "<h2>This is a test email</h2>"

    # Record the MIME types of both parts - text/plain and text/html.
    part1 = MIMEText(text, 'plain')
    part2 = MIMEText(html, 'html')

    # Attach parts into message container.
    # According to RFC 2046, the last part of a multipart message, in this case
    # the HTML message, is best and preferred.
    msg.attach(part1)
    msg.attach(part2)

    # Send the message via local SMTP server, port 587
    print "Server is ", server, port
    s = smtplib.SMTP( server, int(port) )
    if user:
        s.login( user, passwd )

    # sendmail function takes 3 arguments: sender's address, recipient's address
    # and message to send - here it is sent as one string.
    s.sendmail(fromAddr, toAddr, msg.as_string())
    s.quit()