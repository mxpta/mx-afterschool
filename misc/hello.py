from flask import Flask, render_template, url_for

from flask.ext.sqlalchemy import SQLAlchemy

from flask.ext.admin import Admin, BaseView, expose
from flask.ext.admin.contrib.sqla import ModelView



app = Flask( __name__ )
app.config['SECRET_KEY'] = '123456789'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/test.db'
db = SQLAlchemy(app)

# -----------------------------------------------------------
#  Data Model
# -----------------------------------------------------------

course_users = db.Table('course_users', 
	db.Column( 'user_id', db.Integer, db.ForeignKey( 'user.id')),
	db.Column( 'course_id', db.Integer, db.ForeignKey('course.id'))
	)

class User(db.Model):
	id = db.Column( db.Integer, primary_key=True )
	username = db.Column(db.String(80), unique=True)
	email = db.Column(db.String(120), unique=True)

	courses = db.relationship( 'Course', secondary=course_users, 
		backref=db.backref('users',lazy='dynamic') )
	# def __init__( self, username, email):
	# 	self.username = username
	# 	self.email = email

	def __repr__(self):
		return '<User %r>' % self.username

	def __unicode__(self):
		return self.username

class Course( db.Model ):
	id = db.Column( db.Integer, primary_key=True )
	title = db.Column(db.String(100), unique=True)
	weekday = db.Column( db.Enum("Monday", "Tuesday", "Wednesday", 
		"Thursday", "Friday", name='weekday_enum'), nullable=False)
	starttime = db.Column( db.Time(timezone=True))	
	duration = db.Column( db.Integer() )
	instructor = db.Column( db.String(80) )
	summary = db.Column( db.String(300) )
	desc = db.Column( db.Text() )
	capacity = db.Column( db.Integer() )
	
	def __unicode__(self):
		return self.title

def reset_db():
	db.drop_all()
	db.create_all()

	for username, email in [
		( 'admin', 'admin@example.com'),
		( 'guest', 'guest@example.com'),
		( 'joeld', 'joeld42@gmail.com'),
		( 'vczdavis', 'vczdavis@gmail.com') ]:
		user = User()
		user.username = username
		user.email = email
		db.session.add( user )

	db.session.commit()

	for desc, weekday in [
		( 'Fairytale Dance', 'monday'),
		( 'Lego Fun', 'monday'),		
		( 'Painting', 'tuesday'),				
		( 'Class 4', 'wednesday')]:
		course = Course()
		course.title = desc
		course.weekday = weekday
		db.session.add( course )

	db.session.commit()


# -----------------------------------------------------------
#  Admin
# -----------------------------------------------------------

class HelloAdminView(BaseView):
	@expose('/')
	def index(self):
		return self.render('adm_hello.html')

# Admin interface
admin = Admin( app )
admin.add_view( HelloAdminView(name='Hello'))
admin.add_view( ModelView( User, db.session))
admin.add_view( ModelView( Course, db.session))

# -----------------------------------------------------------
#  App Routes
# -----------------------------------------------------------

@app.route("/hello/")
@app.route("/hello/<name>")
def hello(name=None):
	return render_template( 'hello.html', name=name)

if __name__=='__main__':
	app.run( debug=True )