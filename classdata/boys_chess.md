There is a thriving chess community at Malcolm X and the more advanced students who 
sign up regularly continue to break new ground. Last year they learned, among other 
things, three new chess openings, more subtle middle game combinations, three and 
even four moves deep, and a few standard checkmate positions. This year the Boy’s 
class will play the Girl’s class, one or two moves per week, on a magnetic chess 
board that I’ve mounted on the cafeteria wall. Each group looks forward to 
clobbering the other.
