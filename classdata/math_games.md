“We don’t do any math”, reports one Kindergartener, “All we do is play games the 
whole time.” But they do do math, and lots of it. These games the children play are 
the quintessence of math, they are what I wish I had learned in kindergarten. In 
addition, the math is wrapped up inside Grimm’s fairy tales turned into challenges: 
how to escape from trolls chasing you, how to count the stars, how to tell if the 
Wolf is lying. Children frequently ask at the beginning of each class, “What is our 
challenge this week?” That’s the spirit! The instructor keeps a small portfolio – 
some of their drawings or photographs of their work – to give to parents at the end, 
along with an explanation of some of the thinking going on inside their not-so-
small minds.
