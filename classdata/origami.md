Paper folding fun abounds in this introductory class to origami-the Japanese Art of 
paper folding. We'll cover the basic folds and bases to build traditional origami 
toys and games. Next will explore the math/origami relationship by working on simple 
modular (many pieces) models. Each class will have an origami related story and a 
snack will be provided. The last class of the series will be an "origami open house" 
where students will showcase their work. No origami experience is necessary, but 
attention to detail and willingness to "try again" are important! 
