Science Through Toys
--------------------

In these exciting hands-on science classes we will build a variety of toys and use 
them to demonstrate major scientific concepts such as energy, electricity, light, 
gravity, motion, friction, magnetism and simple machines. Each student will 
construct and take home a science toy in every class. The classes will be taught by 
our award-winning teachers. 

Each student will make and take home:
* Solar Powered Buzzing Bug
* Camouflage Catapult
* Zippy Zappy Electrical Robot
* Mix-it-Up Magneto Man
* And more...

About Sarah's Science
----------------------
Sarah's Science was founded in 1987 by Sarah Shaffer to provide a unique and exciting
approach to the wonderful world of science and nature.

Our philosophy is that children learn best by doing. All of our programs feature hands-on 
projects built by each child to engage and delight them while making it easy to understand
even complex science concepts.