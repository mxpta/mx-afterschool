Ultimate, also known as Ultimate Frisbee, is a fun, challenging, inclusive team 
sport. Ultimate was invented in the late ’60s, and today is played in 65 countries 
around the world and recently was named the fastest growing team sport in 
America by the Sporting Goods Manufacturer Association (SGMA). 

Based on sportsmanship, respect, and conflict resolution skills, it teaches character 
and leadership; it is historically a coed sport where girls and boys play together 
even at the highest levels; and it is a safe, non-contact sport where skill is as 
important as athleticism. Non-incidental contacts are fouls and a disc space 
between players is required at all times. Bay Area Disc Association curriculum 
includes 
- Ultimate skills: pivoting, catching, throwing, marking, cutting, faking, defending, pulling; 

- Cognitive skills: teamwork, conflict resolution, leadership, sportsmanship. 
