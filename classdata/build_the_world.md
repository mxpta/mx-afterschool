In this exciting crafts course for kindergarteners, students will build and animate 
a world. They will start with a mass of clay and sand which they will sculpt with 
their hands into land, mountains, a river, a cave. Weaving with twigs, they will 
make a bridge, a fence, and stonework to make huts. Using a wet felt technique, they 
will create animals and people. All this the children can do largely on their own, 
with solid materials, no veneers, no tape or glue, just hand manipulation. And each 
week the instructor will tell them a creation myth from around the world as 
together, they shape their world.
