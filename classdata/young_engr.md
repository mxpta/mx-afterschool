Each day we offer an exciting engineering project for girls and boys to explore. 
After a brief guided lecture, young engineers spend most of the time designing, 
building and testing their creations. Craft wood, wheels, rubber bands, and 
propellers are some of the materials that students will use. Young engineers will 
also use small hot glue guns for many of the projects. Hands-on projects, boosting 
critical thinking skills, and having a blast is what this class is all  about. Best 
of all, students get to take home everything they make!
