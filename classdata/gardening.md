We'll look at the plant life cycle and learn about how the cycles continue and 
change through the seasons. We will also learn about the creatures that live in our 
garden habitat: worms, bugs, butterflies...Down and dirty digging, fresh veggie 
tasting, lots of other fun garden crafts and interactive games – all taking place in 
the great outdoors. Perfect for kids who need outside time after a day spent at 
their desks indoors.
