Why separate chess classes for boys and girls? Since separating boys and girls 
several years ago, we now have equal numbers captivated by the game; most chess 
programs have five to ten times as many boys as girls. The rules and strategies are 
the same for all, but how they are taught can be different, and over the years the 
boys' class has had the freedom to become more stereotypically boyish, and the girls 
– well I have asked them each year if they'd like to keep it separate from the boys, 
and each year the answer is a resounding “Yes!” You can imagine the reasons. There 
is a thriving chess community at Malcolm X and the more advanced students who sign 
up regularly continue to break new ground. 
