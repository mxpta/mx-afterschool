Capoeira is an Afro-Brazilian cultural art form that combines martial arts, dance,
acrobatics and music. Come and join us as we flip, cartwheel, and sing for the 
2014-2015 school year. Children will learn the basic movements, attacks, and
defenses in this class. Students will be barefoot for this class and need to wear
loose fitting pants or shorts. They will do an exhibition of the moves and music
they learned for parents and peers at the end of the session. 
