Cooking around the world gives boys and girls the opportunity to learn about 
countries around the world by cooking and eating! Children learn cooking skills 
while experiencing a country’s culinary flavors. Dishes include creamy Italian 
pesto, Israeli potato, carrot and zucchini latkes, Mexican tortilla soup and 
Canadian butternut squash and maple mac and cheese. Cooking around the world is a 
nut-free program. 
