Using role-playing exercises, students will explore the development of characters, 
plot, and storyline planning, and evolve the group story from introduction through 
conclusion. In Roleplaying Adventure, we imagine a story together that takes place 
in a world of magic and monsters. Using classic paper-and-dice roleplaying, students 
develop their own characters and help build stories as limitless as their 
imaginations. Roleplaying Adventure is a fun way to develop reading and math skills, 
and encourages teamwork, creative problem-solving, imaginative thinking, and good 
listening!
