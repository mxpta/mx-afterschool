#!/bin/env python
# -*- coding: utf-8 -*-

# =========================================
# Malcom-X Elementary School
# PTA Afterschool registration
# =========================================
import os, sys, string, math, random
import datetime
import logging

import markdown

from flask import Flask, render_template, url_for, abort, Markup, request, session, redirect
from flask import escape
from flask.ext.sqlalchemy import SQLAlchemy
from sqlalchemy import or_, and_

from mxafs import model
from mxafs import admin
from mxafs import mxemail
from mxafs import mxinvoice

import stripe

# -----------------------------------------------------------
# Config and init app
# -----------------------------------------------------------
app = Flask(__name__)
app.config.from_envvar( 'MXPTA_SETTINGS' )

#app.config['SECRET_KEY'] = '123456789'
#app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:////tmp/mxafs.db'

# Remove any existing loggers
while len(logging.root.handlers):
    logging.root.removeHandler( logging.root.handlers[-1])

# Init logging
logging.basicConfig( filename=app.config['LOGFILE'],
                     format='%(asctime)s %(levelname)s: %(message)s',
                     level=logging.DEBUG )

logging.info("Application Restarted...")


# DBG: also log to console
ch = logging.StreamHandler(sys.stdout)
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s %(levelname)s: %(message)s')
ch.setFormatter(formatter)

logging.getLogger().addHandler(ch)

# Set your secret key: remember to change this to your live secret key in production
# See your keys here https://dashboard.stripe.com/account
stripe.api_key = app.config['STRIPE_KEY']

# configure mail server
mxemail.setServer( app.config['SMTP_SERVER'], app.config['SMTP_PORT'],
                   app.config.get('SMTP_USER', None),
                   app.config.get('SMTP_PASS', None) )

model.db.app = app
model.db.init_app(app )

# Set up admin interface
admin.init_admin(app)


# -----------------------------------------------------------
#  Utilities
# -----------------------------------------------------------
def userFromSession():
    """Returns User for the current session, or None"""
    user = None
    if 'userId' in session:
        try:
            userId = int(escape(session['userId']))
            user = model.User.query.get(userId)
        except ValueError:
            pass

    return user


def currentStudent(user):
    """Returns the currently active student for the user"""
    students = user.students

    if (len(students) == 0):
        return None

    activeStudent = students[0]  #default to first student

    if 'activeStudent' in session:
        try:
            studentId = int(escape(session['activeStudent']))
        except ValueError:
            pass

        # get the first matching one, not querying for studentId directly
        # so we can limit visibility to this user's students
        for s in students:
            if s.id == studentId:
                activeStudent = s
                break

    return activeStudent

def lookupStudentForUser( user, studentId ):

    # get the first matching one, not querying for studentId directly
    # so we can limit visibility to this user's students
    for s in user.students:
        if s.id == studentId:
            return s

    return None


def setCurrentStudent(activeStudent):
    session['activeStudent'] = activeStudent

def daterange(start_date, end_date):
    for n in range(int ((end_date - start_date).days + 1)):
        yield start_date + datetime.timedelta(n)


# -----------------------------------------------------------
#  App Routes
# -----------------------------------------------------------
@app.route('/')
def index():
    courses = model.CourseInfo.query.filter( model.CourseInfo.courses.any() )

    courses = list(courses)
    random.shuffle(courses)

    # some random pictures to add variety to the front page
    frontImgs = {1: "frontpage_mosaic.jpg",
                 3: "frontpage_shadow.png",
                 4: "frontpage_ceramics.jpg",
                 7: "frontpage_legobot.png",
                 9: "frontpage_rock.jpg",
                 13: "frontpage_rainbowcards.png",
                 17: "frontpage_legobot2.png",
                 }

    grid = gridClassesForAllClasses()

    # Query
    settings = model.getSettings()

    return render_template('index.html',
                           courses=courses, user=userFromSession(),
                           calendar=model.calendarDates(),
                           settings=settings,
                           imgs=frontImgs, grid=grid, nav="home" )


@app.route('/course/<slug>')
def course(slug):
    cinfo = model.CourseInfo.query.filter( model.CourseInfo.slug == slug ).first()
    if not course:
        abort(404)

    # render markdown
    if cinfo.desc:
        desc = Markup(markdown.markdown(cinfo.desc))
    else:
        desc = "No course description available."

    # Get sessions offered
    sessions = cinfo.courses

    # group instructors and markdown their bios
    insts = set()
    for ss in sessions:
        if ss.instructor:
            insts.add( ss.instructor )
    instinfo = []
    for inst in insts:
        if inst.bio:
            instbio = Markup(markdown.markdown(inst.bio))
        else:
            instbio = None

        instinfo.append( (inst, instbio) )


    return render_template('course.html', cinfo=cinfo, sessions=sessions, desc=desc, instinfo=instinfo, user=userFromSession() )


def compareCourse(a, b):

    wdA = model.WEEKDAYS.index(a.weekday)
    wdB = model.WEEKDAYS.index(b.weekday)
    if wdA < wdB:
        return -1
    elif wdA > wdB:
        return 1
    else:
        # weekdays equal, use id
        return a.id - b.id


@app.route('/account')
def account():
    user = userFromSession()

    if not user:
        return "You are not logged in. Visit the link sent to your account to login."

    # Get a list of registered courses
    regReqsForStu, totalDue = mxinvoice.gatherInvoicedCoursesForUser(user)

    # have they been enrolled yet?
    enrolled = False
    for req in regReqsForStu:
        if req.status == 'registered' or req.status=='enrolled':
            enrolled = True
            break

    # Get a list of minical events
    minicalEvents = list(model.CalendarEvent.query.filter( model.CalendarEvent.minical == True ))
    minicalEvents.sort( key=lambda ev: ev.date )

    # Account want the public key for payments
    pubkey = app.config['STRIPE_PUBKEY']

    settings = model.getSettings()

    return render_template('account.html', nav="account", user=user, reqs=regReqsForStu,
                           weekdays=model.WEEKDAYS, total=totalDue, pubkey=pubkey,
                           enrolled = enrolled, minical=minicalEvents,
                           settings=settings,
                           calendar= model.calendarDates() )


def gridClassesForStudent( student ):
    courseList = model.Course.query.filter( and_( model.Course.maxgrade >= student.grade,
                                                  model.Course.mingrade <= student.grade ) )
    gridCourses = {}
    for w in model.WEEKDAYS:
        gridCourses[w] = []

    for c in courseList:
        gridCourses[ c.weekday ].append( c )

    return gridCourses

def gridClassesForAllClasses():

    courseList  = model.Course.query.all()

    gridCourses = {}
    for w in model.WEEKDAYS:
        gridCourses[w] = []

    for c in courseList:
        gridCourses[ c.weekday ].append( c )

    return gridCourses

def requestInSlot( student, slotNdx ):
    reqs = list(student.requests)

    if len(reqs) > slotNdx:
        return reqs[slotNdx]

    return None



@app.route('/changedays' )
def changedays():
    """Resets enrolldays for the current student, then redirect to enroll page to pick"""
    user = userFromSession()
    if not user:
        return "You are not logged in. Visit the link sent to your account to login."

    # Get the (possibly updated) current student
    student = currentStudent(user)
    if not student:
        # If they don't have any students added, send them to
        # the account page to create one
        return redirect(url_for('account'))

    # Reset enrolldays for student
    student.enrollDays = None
    model.db.session.commit()

    # delete any existing CRs
    deleteRequestsForStudent(student)

    return redirect(url_for('enroll'))

@app.route('/actstu/<int:stu_id>')
def actstu(stu_id):
    setCurrentStudent(stu_id)

    return redirect(url_for('enroll'))

@app.route('/enroll/<int:stu_id>', methods=['GET', 'POST'])
@app.route('/enroll/<int:stu_id>/<int:slot>', methods=['GET', 'POST'])
def enroll( stu_id, slot=None ):

    # Make sure someone is logged in
    user = userFromSession()
    if not user:
        return statusPage( 'warning', "Not logged in",
                       "You are not logged in. Visit the link sent to your account to login." )

    # Get the student we're enrolling for
    student = lookupStudentForUser( user, stu_id )
    if not student:
        # If they don't have any students added, send them to
        # the account page to create one
        return redirect(url_for('account'))

    # Editing a current request in this slot?
    currRequestInSlot = None
    if (slot):
        currRequestInSlot = requestInSlot( student, int(slot)-1 )


    courseListAll = model.Course.query.filter( and_( model.Course.maxgrade >= student.grade,
                                                  model.Course.mingrade <= student.grade ) )

    altCourseList = list(courseListAll)
    altCourseList.sort(cmp=compareCourse)

    courseList = []


    # Filter course list, remove any days they have picked as first choice
    for c in courseListAll:

        useCourse = True
        for cr in student.requests:

            # don't filter day if we're editing it
            if (currRequestInSlot) and (currRequestInSlot.course.weekday == c.weekday):
                continue

            # Otherwise, filter this course if we're already matching it
            if (cr.course.weekday == c.weekday):
                useCourse = False
                break

        if useCourse:
            courseList.append( c )

    # Sort them
    courseList.sort(cmp=compareCourse )

    # if slot, look up the current courses for that slot
    currPri = None
    currAlt = None
    if (slot):
        cr = currRequestInSlot
        if cr:
            currPri = cr.course.id
            if cr.altcourse:
                currAlt = cr.altcourse.id

    # Show the enroll page
    return render_template('enroll.html',
                           user=user, student=student, slot=slot,
                           courseList=courseList,
                           altCourseList=altCourseList,
                           currsel = ( currPri, currAlt ),
                           grid=gridClassesForStudent(student) )

@app.route('/delreq/<int:stu_id>/<int:slot>', methods=['GET', 'POST'])
def deleteReq( stu_id, slot ):

    # Make sure someone is logged in
    user = userFromSession()
    if not user:
        return statusPage( 'warning', "Not logged in",
                       "You are not logged in. Visit the link sent to your account to login." )

    # Get the student we're enrolling for
    student = lookupStudentForUser( user, stu_id )
    if student:
        cr = requestInSlot( student, int(slot)-1 )

        if (cr):
            model.db.session.delete(cr)
            model.db.session.commit()

    return redirect(url_for('account'))

@app.route('/enroll/submit/<int:stu_id>', methods=['POST'])
@app.route('/enroll/submit/<int:stu_id>/<int:slot>', methods=['POST'])
def enroll_submit( stu_id, slot=None ):

    # Make sure someone is logged in
    user = userFromSession()
    if not user:
        return statusPage( 'warning', "Not logged in",
                       "You are not logged in. Visit the link sent to your account to login." )

    # Get the student we're enrolling for
    student = lookupStudentForUser( user, stu_id )
    if not student:
        # If they don't have any students added, send them to
        # the account page to create one
        return redirect(url_for('account'))

    primary = request.form['first']
    second = request.form['second']
    if (primary=='None') and (second=='None'):
        # TODO: delete cr if existing
        return redirect(url_for('account'))
    elif (primary=='None'):
        # If for some reason they only set alternate, swap them
        primary = second
        second = 'None'

    # Find or create a course request for this
    cr = None
    if (slot):
        cr = requestInSlot( student, int(slot)-1 )

    if not cr:
        cr = model.CourseRequest()

    cr.status = "requested"
    cr.student = student
    cr.course = model.Course.query.get(int(primary))
    if (second != 'None'):
        cr.altcourse = model.Course.query.get(int(second))
    else:
        cr.altcourse = None

    model.db.session.add(cr)
    model.db.session.commit()

    # And send them back to the account page
    return redirect(url_for('account'))


# Add or edit student
@app.route('/student', methods=[ 'GET', 'POST'])
@app.route('/student/<int:stu_id>', methods=[ 'GET', 'POST'])
def student( stu_id=None ):

    #for k, v in request.form.iteritems():
    #    print k, v

    # Make sure someone is logged in
    user = userFromSession()
    if not user:
        return statusPage( 'warning', "Not logged in",
                       "You are not logged in. Visit the link sent to your account to login." )

    # Get the student information, if specified
    student = None
    if (stu_id):
        student = lookupStudentForUser( user, stu_id )


    # Apply the changes if it's a POST
    if request.method == 'POST':
        firstname = request.form.get('firstname')
        lastname = request.form.get('lastname')
        grade = int(request.form.get('grade'))

        newStu = False
        if not student:
            newStu = True
            student = model.Student()

        student.user = user
        student.firstname = firstname
        student.lastname = lastname
        student.grade = grade

        classroom = model.Classroom.query.get(int(request.form.get('classroom')) )
        #print "Classroom is ", classroom
        if classroom:
            student.classroom = classroom

        student.bears = (request.form.get('bears', 0) == 'on')
        student.learns = (request.form.get('learns', 0) == 'on')

        if newStu:
            model.db.session.add(student)
        model.db.session.commit()

        # Did they want to add more students?
        if (request.form.has_key('addmore')):
            # If so, trigger the student page again
            return redirect(url_for('student'))
        else:
            # Otherwise, redirect back to account page
            return redirect(url_for('account'))
    else:

        # GET request, render the student form

        # Get a list of classrooms for teacher picker
        classrooms = model.Classroom.query.all()

        return render_template('student.html',
                               user=user, student=student, classrooms=classrooms )


@app.route('/user/<int:user_id>', methods=[ 'GET', 'POST'])
def user( user_id ):

    #for k, v in request.form.iteritems():
    #    print k, v

    # Make sure someone is logged in
    user = userFromSession()

    # Make sure the user info matches the signed in user
    if (not user) or (user.id != user_id):
        return statusPage( 'warning', "Not logged in",
                       "You are not logged in. Visit the link sent to your account to login." )

    # Apply the changes if it's a POST
    if request.method == 'POST':
        #print "Applying user"
        user.firstname = request.form.get('firstname')
        user.lastname = request.form.get('lastname')
        user.phone = request.form.get('phone')
        user.email = request.form.get('email')

        user.scholarship = (request.form.get('scholarship', 0) == 'on')
        user.scholrequest = request.form.get('scholrequest')
        user.scholnote = request.form.get('scholnote')

        model.db.session.commit()

        return redirect(url_for('account'))
    else:
        # GET, show user page
        return render_template('user.html', user=user )

# @app.route('/payment', methods=['POST'])
@app.route('/payment', methods=['POST', 'GET'])
def payment():

    user = userFromSession()

    if not user:
        return "You are not logged in. Visit the link sent to your account to login."

    # Get a list of registered courses
    regReqsForStu, totalDue = mxinvoice.gatherInvoicedCoursesForUser(user)

    stripeToken = request.form['stripeToken']
    stripeEmail = request.form['stripeEmail']
    stripeTokenType = request.form['stripeTokenType']
    chargeAmount = int(totalDue * 100)
    try:
        charge = stripe.Charge.create(
            amount=chargeAmount,
            currency="usd",
            card=stripeToken,
            description=user.email)

    except stripe.CardError, e:
        logging.error( "Payment error: user %s amount %d - %s", user.displayname(), chargeAmount, str(e) )

        return statusPage( 'danger', 'Payment Problem.',
                           "There was an error processing your card: <pre>" + str(e) + "</pre>"+
                           'Try again, or contact the <a href="' + url_for('contact') + '">PTA Afterschool Coordinators</a> to '+
                           'pay by check. Thank you!',
                           btn='Back', dest=url_for('account') )


    # Payment succeeded. Mark classes as enrolled and create a payment object
    stripeInfo = ( stripeTokenType, stripeToken, stripeEmail )
    mxinvoice.processPayment( user, totalDue, stripeInfo, regReqsForStu )

    # Send success email
    regReqsForStu, totalDue = mxinvoice.gatherInvoicedCoursesForUser(user)
    if (totalDue == 0):
        # Total Due should be zero here, send a receipt email
        mxemail.sendReceiptEmail(user, regReqsForStu, calendar=model.calendarDates() )

    # Present successfuly status
    return statusPage( 'success', 'Payment Succeeded.','You are now enrolled. Thanks for participating in '+
                       'the Afterschool Enrichment Program at Malcolm X!',
                       icon='ok' )



@app.route('/login')
def signup():
    return render_template('signup.html')


@app.route('/signup_submit', methods=['POST'])
def signup_submit():
    #for k, v in request.form.iteritems():
    #    print k, v

    # Check that the email is not already used
    user = model.User.query.filter(model.User.email == request.form['email']).first()
    if user:
        return statusPage('danger', "Email Used",
                           ("That email has already been used. Use a different email or use "+
                            "the link at the bottom of the sign-in page to resend your login email."),
                            dest=url_for('signup'), btn="OK", icon=None )

    # Create a user
    user = model.User()
    user.lastname = request.form['lastname']
    user.firstname = request.form['firstname']
    user.email = request.form['email']
    user.phone = request.form['phone']
    user.passwd = model.genPasswd()

    model.db.session.add(user)
    model.db.session.commit()

    logging.info( 'Created user %s (#%d)', user.email, user.id )

    # Set new user as the current user
    session['userId'] = user.id

    # send welcome email
    mxemail.sendWelcomeEmail(user, model.calendarDates() )

    return redirect(url_for('student'))


# This is not the bestest security in the world since it relies on emailing
# login links, but should do all right for pta signups
@app.route('/login/<int:userId>')
def login(userId):
    auth = request.args.get('auth')

    targ = request.args.get('targ')

    # No auth key given
    if not auth:
        session.pop('userId', None)
        return redirect(url_for('index'))

    user = model.User.query.get(userId)

    if not user:
        return statusPage( 'warning', "Unknown User",
                       "<h5>This user cannot be found.</h5> NOTE: User data is currently not kept between semesters. If you " +
                       "are trying to log in with a link from a previous semester, you'll have to re-create your account. "+
                       "We apologize for the inconvenience.")

    if user.passwd == auth:
        session['userId'] = userId
    else:
        logging.warning("Bad auth for user: %s (#%d), auth %s", user.displayname(), user.id, auth )
        session.pop('userId', None)

        return statusPage( 'warning', "Invalid Login Link",
                       "<h5>This user cannot be found.</h5> NOTE: User data is currently not kept between semesters. If you " +
                       "are trying to log in with a link from a previous semester, you'll have to re-create your account. "+
                       "We apologize for the inconvenience.")


    if targ == 'acct':
        return redirect(url_for('account'))
    elif targ == 'enroll':
        return redirect(url_for('enroll'))
    else:
        return redirect(url_for('index'))


@app.route('/logout')
def logout():
    # Remove userid from session cookie
    session.pop('userId', None)
    return redirect(url_for('index'))


@app.route('/remind/<int:userId>')
def remind(userId):

    user = model.Course.query.get(userId)
    if (user):
        mxemail.sendRemindEmail(user)

    return redirect(url_for('index'))


@app.route('/remindmail', methods=['POST'])
def remindmail():

    user = None
    email = request.form.get('email', None)

    if email:
        user = model.User.query.filter(model.User.email == email).first()

    if (user):
        mxemail.sendRemindEmail(user)
        return statusPage( 'success', 'Email Sent.','Login email has been sent. Check your email for a login link. Thank you.',
                       icon='ok' )

    else:
        logging.warning( "Remind mail requested for '%s' but that email not found.", email )

        return statusPage( 'danger', 'Email not found','That email was not found in the system. '+
                           'Try again, or contact the <a href="' + url_for('contact') + '">PTA Afterschool Coordinators</a> if '+
                           'you need help.',
                     dest=url_for('signup'), btn="OK", icon=None )

    return redirect(url_for('index'))

@app.route( '/contact')
def contact():
    return render_template( 'contact.html', user=userFromSession(), nav="contact" )

@app.route( '/help')
def help():
    return render_template( 'help.html', user=userFromSession(), nav="faq" )


@app.route( '/faq')
def faq():
    allevents = list(model.CalendarEvent.query.all())
    calendar = []
    holidays = []

    for ev in allevents:
        if ev.role=="Holiday":
            holidays.append( ev )
        else:
            calendar.append( ev )

    calendar.sort( key=lambda ev: ev.date )
    holidays.sort( key=lambda ev: ev.date )

    # Get all the weekdays that we have classes on
    classWeekdays = set()
    for course in model.Course.query.all():
        classWeekdays.add( course.weekday )

    # Use WEEKDAYS to preserve order
    weekdays = []
    for wkd in model.WEEKDAYS:
        if wkd in classWeekdays:
            weekdays.append( wkd )

    classDaysForWeekday = {}
    firstClassDay = None
    lastClassDay = None
    for day in calendar:
        if  day.role == 'FirstDay':
            firstClassDay = day
        elif day.role == 'LastDay':
            lastClassDay = day

    if firstClassDay and lastClassDay:
        for classDay in daterange( firstClassDay.date, lastClassDay.date ):

            # See if this date is in holidays
            isHoliday = False
            for h in holidays:
                if h.date.date() == classDay.date():
                    isHoliday = True
                    break

            if isHoliday:
                continue

            # Put in the appropriate weekday bin
            wkd = classDay.strftime('%A')
            if not classDaysForWeekday.has_key( wkd ):
                classDaysForWeekday[wkd] = []

            classDaysForWeekday[ wkd ].append( classDay )

    return render_template( 'faq.html', user=userFromSession(), nav="faq",
                            calendar=calendar, holidays=holidays,
                            weekdays=weekdays, classDays=classDaysForWeekday )


@app.route( '/test_status/')
def test_status():
    return statusPage( 'warning', "This is a test warning",
                       "This is a test warning for the message page." )


def statusPage( context, title, message, dest=None, btn="OK", icon=None ):

    if not dest:
        dest = url_for("index")

    return render_template( 'status.html',
                            context=context, title=title,
                            message=message, button=btn,
                            icon=icon,
                            dest=dest )

# -----------------------------------------------------------
#  Admin stuff
# -----------------------------------------------------------

@app.route('/emailtest/')
def email():
    user = userFromSession()
    if not user:
        return "You are not logged in. Visit the link sent to your account to login."

    mxemail.sendWelcomeEmail(user, model.calendarDates() )

    return '<h1>Welcome email has been sent...</h1>'


@app.route('/emailtest2/')
def email2():
    user = userFromSession()
    if not user:
        return "You are not logged in. Visit the link sent to your account to login."

    # Get a list of registered courses
    regReqsForStu, totalDue = \
        mxinvoice.gatherInvoicedCoursesForUser(user)

    # Send the email
    #mxemail.sendInvoiceEmail(user, regReqsForStu, totalDue, calendar=model.calendarDates() )
    mxemail.sendReceiptEmail(user, regReqsForStu, calendar=model.calendarDates() )

    return '<h1>Invoice email has been sent...</h1>'


# -----------------------------------------------------------
# main for command line
# -----------------------------------------------------------
if __name__ == '__main__':

    # Uncomment these lines to initialize the database
    #model.rebuild_db2()
    #sys.exit(1)

    # Set up admin interface
    #admin.init_admin(app)

    app.run(host='0.0.0.0', debug=True)
    #app.run( debug=True)
